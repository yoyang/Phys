/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {
  class CaloCluster;
  class Particle;
} // namespace LHCb

/** @class ICloneCaloCluster MicroDST/ICloneCaloCluster.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2010-08-13
 */
class GAUDI_API ICloneCaloCluster : virtual public MicroDST::ICloner<LHCb::CaloCluster> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneCaloCluster, 1, 0 );

  /// Destructor
  virtual ~ICloneCaloCluster() {}

public:
  /// clone a cluster with parent info
  virtual LHCb::CaloCluster* clone( const LHCb::CaloCluster* hypo, const LHCb::Particle* parent = nullptr ) = 0;
};
