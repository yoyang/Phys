/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ICloneMCVertex.h,v 1.3 2009-07-29 21:29:01 jpalac Exp $
#ifndef MICRODST_ICLONEMCVERTEX_H
#define MICRODST_ICLONEMCVERTEX_H 1

// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {
  class MCVertex;
}

/** @class ICloneMCVertex MicroDST/ICloneMCVertex.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */
class GAUDI_API ICloneMCVertex : virtual public MicroDST::ICloner<LHCb::MCVertex> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneMCVertex, 2, 0 );

  /// Destructor
  virtual ~ICloneMCVertex() {}
};

#endif // MICRODST_ICLONEMCVERTEX_H
