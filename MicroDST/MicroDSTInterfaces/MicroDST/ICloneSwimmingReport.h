/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ICloneSwimmingReport.h,v 1.1 2009-07-29 21:28:36 jpalac Exp $
#ifndef MICRODST_ICLONESWIMMINGREPORT_H
#define MICRODST_ICLONESWIMMINGREPORT_H 1

// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {
  class SwimmingReport;
}

/** @class ICloneSwimmingReport MicroDST/ICloneSwimmingReport.h
 *
 *
 *  @author Roel Aaij
 *  @date   2011-10-09
 */
class GAUDI_API ICloneSwimmingReport : virtual public MicroDST::ICloner<LHCb::SwimmingReport> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneSwimmingReport, 1, 0 );

  /// Destructor
  virtual ~ICloneSwimmingReport() {}
};

#endif // MICRODST_ICLONESWIMMINGREPORT_H
