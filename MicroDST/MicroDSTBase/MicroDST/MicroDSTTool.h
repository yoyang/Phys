/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MicroDSTTool.h,v 1.4 2009-07-30 10:05:04 jpalac Exp $
#ifndef MICRODST_MICRODSTTOOL_H
#define MICRODST_MICRODSTTOOL_H 1

// Include files
// from Gaudi
#include <GaudiAlg/GaudiTool.h>
#include <MicroDST/MicroDSTCommon.h>

/** @class MicroDSTTool MicroDSTTool.h MicroDST/MicroDSTTool.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-12-04
 */
class GAUDI_API MicroDSTTool : public MicroDSTCommon<GaudiTool> {

public:
  /// Standard constructor
  MicroDSTTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  virtual ~MicroDSTTool(); ///< Destructor
};

#endif // MICRODST_MICRODSTTOOL_H
