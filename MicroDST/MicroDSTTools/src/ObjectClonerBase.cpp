/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ObjectClonerBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ObjectClonerBase
//
// 2012-03-30 : Chris Jones
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ObjectClonerBase::ObjectClonerBase( const std::string& type, const std::string& name, const IInterface* parent )
    : MicroDSTTool( type, name, parent ) {
  declareProperty( "TESVetoList", m_tesVetoList );
  declareProperty( "TESAlwaysClone", m_tesAlwaysClone );
}

//=============================================================================

StatusCode ObjectClonerBase::initialize() {
  const auto sc = MicroDSTTool::initialize();
  if ( sc.isFailure() ) return sc;

  // sort for find efficiency
  std::sort( m_tesVetoList.begin(), m_tesVetoList.end() );
  std::sort( m_tesAlwaysClone.begin(), m_tesAlwaysClone.end() );

  return sc;
}

//=============================================================================
