/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FLAVOURTAGDEEPCLONER_H
#define FLAVOURTAGDEEPCLONER_H 1

#include "ObjectClonerBase.h"

#include "MicroDST/ICloneFlavourTag.h"

namespace LHCb {
  class FlavourTag;
}

class ICloneParticle;

/** @class FlavourTagDeepCloner FlavourTagDeepCloner.h
 *
 *  Clone an LHCb::FlavourTag
 *  Clone its LHCb::Taggers complete with all their tagger LHCb::Particles.
 *
 *  @author Juan PALACIOS
 *  @date   2009-11-17
 */
class FlavourTagDeepCloner : public extends<ObjectClonerBase, ICloneFlavourTag> {

public:
  /// Standard constructor
  FlavourTagDeepCloner( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  LHCb::FlavourTag* operator()( const LHCb::FlavourTag* tag ) override;

private:
  LHCb::FlavourTag* clone( const LHCb::FlavourTag* tag );

  /**
   * Clone the taggers of an LHCb::FlavourTag and replace the
   * original taggers by the clones. The cloning of the taggers is
   * performed by the cloneTagger method.
   *
   *  @author Juan PALACIOS
   *  @date   2009-11-17
   *
   */
  void cloneTaggers( LHCb::FlavourTag* tag ) const;

  /**
   *
   * Clone and LHCb::Tagger, replacing its tagger particles by clones.
   * The cloning of the tagger particles is performed by cloneParticle.
   *
   *  @author Juan PALACIOS
   *  @date   2009-11-17
   *
   */
  LHCb::Tagger cloneTagger( const LHCb::Tagger& tagger ) const;

  /**
   *
   *  Clone an LHCb::Particle using an IParticleCloner.
   *
   *  @author Juan PALACIOS
   *  @date   2009-11-17
   *
   */
  const LHCb::Particle* cloneParticle( const LHCb::Particle* particle ) const;

private:
  typedef MicroDST::BasicCopy<LHCb::FlavourTag> BasicFTCopy;

  ICloneParticle* m_particleCloner;

  std::string m_particleClonerName;

  bool m_cloneTaggerParticles;
};

//=============================================================================

inline const LHCb::Particle* FlavourTagDeepCloner::cloneParticle( const LHCb::Particle* particle ) const {
  return ( *m_particleCloner )( particle );
}

#endif // FLAVOURTAGDEEPCLONER_H
