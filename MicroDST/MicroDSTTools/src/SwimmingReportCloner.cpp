/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from LHCb
#include "Event/SwimmingReport.h"

// local
#include "SwimmingReportCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SwimmingReportCloner
//
// 2011-10-09 : Roel Aaij
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SwimmingReportCloner::SwimmingReportCloner( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {}

//=============================================================================

LHCb::SwimmingReport* SwimmingReportCloner::operator()( const LHCb::SwimmingReport* report ) {
  return this->clone( report );
}

//=============================================================================

LHCb::SwimmingReport* SwimmingReportCloner::clone( const LHCb::SwimmingReport* report ) {
  return cloneKeyedContainerItem<Cloner>( report );
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( SwimmingReportCloner )
