/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_PARTICLECLONER_H
#define MICRODST_PARTICLECLONER_H 1

#include <algorithm>

#include "ObjectClonerBase.h"

#include <MicroDST/ICloneParticle.h>

#include <MicroDST/ICloneProtoParticle.h>
#include <MicroDST/ICloneVertex.h>

// from LHCb
#include "Event/Particle.h"
#include "Event/Vertex.h"

/** @class ParticleCloner ParticleCloner.h src/ParticleCloner.h
 *
 *  Clone an LHCb::Particle. Recursively clone it's daughter LHCb::Particles.
 *  Depending on the value of the ICloneVertex and ICloneProtoParticle
 *  properties, clone the end vertex and associated LHCb::ProtoParticle.
 *
 *  <b>Oprions</b>
 *  ICloneVertex: string, implementation of ICloneVertex interface used to
 *                clone end vertes. Default = "VertexCloner". If set to "NONE",
 *                only a SmartRef to the end LHCb::Vertex is stored.
 *  ICloneProtoParticle: string, implementation of ICloneVertex interface used
 *                to clone the particle's LHCb::ProtoParticle.
 *                Default = "ProtoParticleCloner". If set to "NONE",
 *                only a SmartRef to the end LHCb::ProtoParticle is stored.
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */
class ParticleCloner : public extends1<ObjectClonerBase, ICloneParticle> {

public:
  /// Standard constructor
  ParticleCloner( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  LHCb::Particle* operator()( const LHCb::Particle* particle ) override;

private:
  LHCb::Particle* clone( const LHCb::Particle* particle );

  void storeDaughters( LHCb::Particle* particleClone, const SmartRefVector<LHCb::Particle>& daughters );

private:
  typedef MicroDST::BasicItemCloner<LHCb::Particle> BasicParticleCloner;

  ICloneVertex* m_vertexCloner = nullptr;

  std::string m_vertexClonerName;

  ICloneProtoParticle* m_ppCloner = nullptr;

  std::string m_ppClonerName;
};

#endif // MICRODST_PARTICLECLONER_H
