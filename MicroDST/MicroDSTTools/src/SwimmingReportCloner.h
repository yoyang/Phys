/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SWIMMINGREPORTCLONER_H
#define SWIMMINGREPORTCLONER_H 1

#include "ObjectClonerBase.h"

#include <MicroDST/ICloneSwimmingReport.h>

namespace LHCb {
  class SwimmingReport;
}

/** @class SwimmingReportCloner SwimmingReportCloner.h
 *
 *  MicroDSTTool that clones an LHCb::SwimmingReport for storage on the MicroDST.
 *
 *  @author Roel Aaij
 *  @date   2010-10-09
 */
class SwimmingReportCloner : public extends<ObjectClonerBase, ICloneSwimmingReport> {

public:
  /// Standard constructor
  SwimmingReportCloner( const std::string& type, const std::string& name, const IInterface* parent );

private:
  LHCb::SwimmingReport* operator()( const LHCb::SwimmingReport* report ) override;

  typedef MicroDST::BasicItemCloner<LHCb::SwimmingReport> Cloner;

  LHCb::SwimmingReport* clone( const LHCb::SwimmingReport* report );
};

#endif // SWIMMINGREPORTCLONER_H
