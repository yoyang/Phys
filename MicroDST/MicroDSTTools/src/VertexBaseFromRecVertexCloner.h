/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VERTEXBASEFROMRECVERTEXCLONER_H
#define VERTEXBASEFROMRECVERTEXCLONER_H 1

#include "ObjectClonerBase.h"

#include <MicroDST/ICloneVertexBase.h>

// local
#include "RecVertexClonerFunctors.h"

/** @class VertexBaseFromRecVertexCloner VertexBaseFromRecVertexCloner.h
 *
 *  MicroDSTTool that clones an LHCb::RecVertex for storage on the MicroDST.
 *  The LHCb::RecVertex's constituent LHCb::Tracks are not cloned for storage.
 *  SmartRefs to them are stored instead.
 *
 *  @author Juan PALACIOS
 *  @date   2007-12-05
 */
class VertexBaseFromRecVertexCloner : public extends<ObjectClonerBase, ICloneVertexBase> {

public:
  /// Standard constructor
  VertexBaseFromRecVertexCloner( const std::string& type, const std::string& name, const IInterface* parent );

  LHCb::VertexBase* operator()( const LHCb::VertexBase* vertex ) override;

private:
  LHCb::RecVertex* operator()( const LHCb::RecVertex* vertex );

  typedef MicroDST::RecVertexClonerShallowTracks PVCloner;

protected:
  virtual LHCb::RecVertex* clone( const LHCb::RecVertex* vertex );
};

#endif // VERTEXBASEFROMRECVERTEXCLONER_H
