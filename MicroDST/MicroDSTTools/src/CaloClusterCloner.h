/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "ObjectClonerBase.h"

#include <MicroDST/Functors.hpp>
#include <MicroDST/ICloneCaloCluster.h>
#include <MicroDST/ICloneCaloDigit.h>

// from LHCb
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"

/** @class CaloClusterCloner CaloClusterCloner.h src/CaloClusterCloner.h
 *
 *  Clone an LHCb::CaloCluster. Deep-clones CaloClusterEntries (CaloDigits and CaloAdc).
 *
 *  @author Juan PALACIOS
 *  @date   2010-08-13
 */

class CaloClusterCloner : public extends<ObjectClonerBase, ICloneCaloCluster> {

public:
  /// Standard constructor
  CaloClusterCloner( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  LHCb::CaloCluster* operator()( const LHCb::CaloCluster* hypo ) override;

  LHCb::CaloCluster* clone( const LHCb::CaloCluster* hypo, const LHCb::Particle* parent = nullptr ) override;

private:
  typedef MicroDST::BasicCopy<LHCb::CaloCluster> BasicCaloClusterCloner;
  typedef MicroDST::BasicCopy<LHCb::CaloDigit>   BasicCaloDigitCloner;

  /// Flag to turn on saving of full information for neutrals
  bool m_cloneEntriesNeuP;

  /// Flag to turn on saving of full information for charged particles
  bool m_cloneEntriesChP;

  /// Flag to always clone associated CaloClusterEntry information, regardless
  /// of parent particle. Overrides NeuP and ChP settings.
  bool m_cloneEntriesAlways;

  /// calo digit cloner
  ICloneCaloDigit* m_caloDigitCloner = nullptr;

  /// Digit status filter
  LHCb::CaloDigitStatus::Status m_status{LHCb::CaloDigitStatus::UseForEnergy | LHCb::CaloDigitStatus::UseForPosition |
                                         LHCb::CaloDigitStatus::UseForCovariance};

  /// Flag to enable the filtering of digits based on the mask
  bool m_mask = true;
};
