###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: PhysSys
################################################################################
gaudi_subdir(PhysSys)

gaudi_depends_on_subdirs(Kernel/SelectionLine
                         MicroDST/MicroDSTAlgorithm
                         MicroDST/MicroDSTBase
                         MicroDST/MicroDSTConf
                         MicroDST/MicroDSTInterfaces
                         MicroDST/MicroDSTTools
                         Phys/BBDecTreeTool
                         Phys/CommonParticles
                         Phys/DaVinciInterfaces
                         Phys/DaVinciKernel
                         Phys/DaVinciMCKernel
                         Phys/DaVinciTools
                         Phys/DSTWriters
                         Phys/ParticleCombiners
                         Phys/RelatedInfoTools
                         Phys/ExtraInfoTools
                         Phys/DaVinciPVTools
                         Phys/DaVinciFilters
                         Phys/DaVinciDecayFinder
                         Phys/TopologicalTools
                         Phys/DaVinciOverlapsAndClones
                         Phys/DaVinciNeutralTools
                         Phys/DaVinciTransporter
                         Phys/DaVinciTypes
                         Phys/DaVinciUtils
                         Phys/DecayTreeFitter
                         Phys/HighPtJets
                         Phys/KalmanFilter
                         Phys/LoKi
                         Phys/LoKiAlgo
                         Phys/LoKiArrayFunctors
                         Phys/LoKiFitters
                         Phys/LoKiJets
                         Phys/LoKiPhys
                         Phys/LoKiProtoParticles
                         Phys/LoKiTracks
                         Phys/LoKiUtils
                         Phys/NeuroBayesTools
                         Phys/ParticleMaker
                         Phys/PhysConf
                         Phys/PhysDict
                         Phys/ProtoParticleFilter
                         Phys/SelPy
                         Phys/Swimming
                         Phys/TeslaTools
                         Phys/TisTosTobbing
                         Phys/VertexFit
                         Phys/FlavourTagging
                         Phys/JetAccessories
                         Phys/JetTagging
                         Phys/MVADictTools
                         PhysSel/PhysSelPython)
