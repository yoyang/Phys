/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Vertices5.h"
#include "LoKi/Constants.h"
#include "LoKi/PhysSources.h"
#include "LoKi/Vertices0.h"
// ============================================================================
/** @file
 *  Collection of "context-dependent" functors, needed for the
 *  new framework "CombineParticles", developed by Juan PALACIOS,
 *   Patrick KOPPENBURG and Gerhard RAVEN.
 *
 *  Essentially all these functord depends on "event-data" and
 *  get the nesessary "context-dependent" data from Algorithm Context Service
 *
 *  The basic ingredients here:
 *   - LoKi Service
 *   - Algorithm Context Service
 *   - PhysDesktop
 *   - LoKi::getPhysDesktop
 *   - Gaudi::Utils::getDValgorithm
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2008-03-28
 */
// ============================================================================
namespace {
  // ==========================================================================
  // the local empty vector of vertices
  static const LHCb::VertexBase::ConstVector s_EMPTY;
  // ==========================================================================
  /// the finder for the primary vertices:
  const LoKi::Vertices::IsPrimary s_PRIMARY = LoKi::Vertices::IsPrimary();
  // ==========================================================================
} // namespace
// ============================================================================
// constructor from the source
// ============================================================================
LoKi::Vertices::MinVertexDistanceWithSource::MinVertexDistanceWithSource(
    const LoKi::Vertices::MinVertexDistanceWithSource::_Source& source )
    : LoKi::AuxFunBase( std::tie( source ) ), LoKi::Vertices::MinVertexDistance( s_EMPTY ), m_source( source ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Vertices::MinVertexDistanceWithSource* LoKi::Vertices::MinVertexDistanceWithSource::clone() const {
  return new LoKi::Vertices::MinVertexDistanceWithSource( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Vertices::MinVertexDistanceWithSource::result_type LoKi::Vertices::MinVertexDistanceWithSource::minvdsource(
    LoKi::Vertices::MinVertexDistanceWithSource::argument v ) const {
  if ( 0 == v ) {
    Error( "LHCb::VertexBase* points to NULL, return 'Huge Distance'" ).ignore();
    return LoKi::Constants::HugeDistance; // RETURN
  }
  //
  // check the event
  if ( !sameEvent() ) {
    typedef LoKi::UniqueKeeper<LHCb::VertexBase> KEEPER;
    const KEEPER&                                keep1 = *this;
    KEEPER&                                      keep2 = const_cast<KEEPER&>( keep1 );
    //
    // clear the list of vertices
    keep2.clear();
    // get the primary vertices from the source
    LHCb::VertexBase::ConstVector primaries = m_source();
    // fill the functor with primary vertices:
    keep2.addObjects( primaries.begin(), primaries.end() );
    if ( empty() ) { Warning( "Empty list of vertices is loaded!" ).ignore(); }
    // update the event:
    setEvent();
  }
  // use the functor
  return minvd( v );
}
// ============================================================================
// OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Vertices::MinVertexDistanceWithSource::fillStream( std::ostream& s ) const {
  return s << "VMINVDSOURCE(" << m_source << ")";
}
// ============================================================================

// ============================================================================
// the default constructor
// ============================================================================
LoKi::Vertices::MinVertexDistanceDV::MinVertexDistanceDV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::Vertices::MinVertexDistanceWithSource( LoKi::Vertices::SourceDesktop( algorithm, s_PRIMARY ) )
    , m_cut( s_PRIMARY ) {}
// ============================================================================
// the constructor form the vertex filter
// ============================================================================
LoKi::Vertices::MinVertexDistanceDV::MinVertexDistanceDV( const IDVAlgorithm*           algorithm,
                                                          const LoKi::PhysTypes::VCuts& cut )
    : LoKi::AuxFunBase( std::tie( algorithm, cut ) )
    , LoKi::Vertices::MinVertexDistanceWithSource( LoKi::Vertices::SourceDesktop( algorithm, cut ) )
    , m_cut( cut ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Vertices::MinVertexDistanceDV* LoKi::Vertices::MinVertexDistanceDV::clone() const {
  return new LoKi::Vertices::MinVertexDistanceDV( *this );
}
// ============================================================================
// OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Vertices::MinVertexDistanceDV::fillStream( std::ostream& s ) const {
  return s << "VMINVDDV(" << m_cut << ")";
}
// ============================================================================

// ============================================================================
// constructor from the source
// ============================================================================
LoKi::Vertices::MinVertexChi2DistanceWithSource::MinVertexChi2DistanceWithSource(
    const LoKi::Vertices::MinVertexChi2DistanceWithSource::_Source& source )
    : LoKi::AuxFunBase( std::tie( source ) ), LoKi::Vertices::MinVertexChi2Distance( s_EMPTY ), m_source( source ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Vertices::MinVertexChi2DistanceWithSource* LoKi::Vertices::MinVertexChi2DistanceWithSource::clone() const {
  return new LoKi::Vertices::MinVertexChi2DistanceWithSource( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Vertices::MinVertexChi2DistanceWithSource::result_type
LoKi::Vertices::MinVertexChi2DistanceWithSource::minvdchi2source(
    LoKi::Vertices::MinVertexChi2DistanceWithSource::argument v ) const {
  if ( 0 == v ) {
    Error( "LHCb::VertexBase* points to NULL, return 'Huge Chi2'" ).ignore();
    return LoKi::Constants::HugeChi2; // RETURN
  }
  // check the event
  if ( !sameEvent() ) {
    typedef LoKi::UniqueKeeper<LHCb::VertexBase> KEEPER;
    const KEEPER&                                keep1 = *this;
    KEEPER&                                      keep2 = const_cast<KEEPER&>( keep1 );
    //
    // clear the list of vertices
    keep2.clear();
    // get the primary vertices from the source
    LHCb::VertexBase::ConstVector primaries = m_source();
    // fill the functor with primary vertices:
    keep2.addObjects( primaries.begin(), primaries.end() );
    if ( empty() ) { Warning( "Empty list of vertices is loaded!" ).ignore(); }
    // update the event:
    setEvent();
  }
  // use the functor
  return minvdchi2( v );
}
// ============================================================================
// OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Vertices::MinVertexChi2DistanceWithSource::fillStream( std::ostream& s ) const {
  return s << "VMINVDCHI2SOURCE(" << m_source << ")";
}
// ============================================================================

// ============================================================================
// the default constructor
// ============================================================================
LoKi::Vertices::MinVertexChi2DistanceDV::MinVertexChi2DistanceDV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::Vertices::MinVertexChi2DistanceWithSource( LoKi::Vertices::SourceDesktop( algorithm, s_PRIMARY ) )
    , m_cut( s_PRIMARY ) {}
// ============================================================================
// the constructor form the vertex filter
// ============================================================================
LoKi::Vertices::MinVertexChi2DistanceDV::MinVertexChi2DistanceDV( const IDVAlgorithm*           algorithm,
                                                                  const LoKi::PhysTypes::VCuts& cut )
    : LoKi::AuxFunBase( std::tie( algorithm, cut ) )
    , LoKi::Vertices::MinVertexChi2DistanceWithSource( LoKi::Vertices::SourceDesktop( algorithm, cut ) )
    , m_cut( cut ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Vertices::MinVertexChi2DistanceDV* LoKi::Vertices::MinVertexChi2DistanceDV::clone() const {
  return new LoKi::Vertices::MinVertexChi2DistanceDV( *this );
}
// ============================================================================
// OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Vertices::MinVertexChi2DistanceDV::fillStream( std::ostream& s ) const {
  return s << "VMINVDCHI2DV(" << m_cut << ")";
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
