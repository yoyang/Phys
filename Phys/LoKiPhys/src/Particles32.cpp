/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Particles32.h"
#include "LoKi/Constants.h"
// ============================================================================
/** @file
 *  Implementation file for classese from file  LoKi/Particles32.h
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 20100219
 */
// ============================================================================
// constructor from vertex-function
// ============================================================================
LoKi::Particles::BestPrimaryVertexAdaptor::BestPrimaryVertexAdaptor( const IDVAlgorithm*           algorithm,
                                                                     const LoKi::PhysTypes::VFunc& vfun )
    : LoKi::AuxFunBase( std::tie( algorithm, vfun ) ), LoKi::AuxDesktopBase( algorithm ), m_vfun( vfun ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::BestPrimaryVertexAdaptor* LoKi::Particles::BestPrimaryVertexAdaptor::clone() const {
  return new BestPrimaryVertexAdaptor( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::BestPrimaryVertexAdaptor::result_type LoKi::Particles::BestPrimaryVertexAdaptor::
                                                       operator()( LoKi::Particles::BestPrimaryVertexAdaptor::argument p ) const {
  if ( UNLIKELY( !p ) ) {
    Error( "LHCb::Particle* point to NULL, return 'NegativeInfinity' " ).ignore();
    return LoKi::Constants::NegativeInfinity;
  }
  // get the best vertex from desktop
  const LHCb::VertexBase* vertex = bestVertex( p );
  //
  return m_vfun( vertex );
}
// ============================================================================
// OPTIONAL: the nice string representation
// ============================================================================
std::ostream& LoKi::Particles::BestPrimaryVertexAdaptor::fillStream( std::ostream& s ) const {
  return s << " BPV( " << m_vfun << " ) ";
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
