/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PHYSSOURCES_H
#  define LOKI_PHYSSOURCES_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#  include "GaudiKernel/IDataProviderSvc.h"
// ============================================================================
// PhysEvent
// ============================================================================
#  include "Event/Particle.h"
// ============================================================================
// DaVinciInterfaces
// ============================================================================
#  include "Kernel/IDVAlgorithm.h"
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/BasicFunctors.h"
#  include "LoKi/Interface.h"
#  include "LoKi/PhysTypes.h"
#  include "LoKi/Sources.h"
#  include "LoKi/TES.h"
// ============================================================================
class DVAlgorithm;
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2001-01-23
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class SourceTES
     *  @see LoKi::Cuts::SOURCE
     *  @see LoKi::Cuts::SOURCETES
     *  simple "source for the particles in TES"
     *  @author Vanya BELYAEV ibelyav@physics.syr.edu
     *  @date 2006-12-07
     */
    class SourceTES
        : public LoKi::Functors::Source<LHCb::Particle, LHCb::Particle::ConstVector, LHCb::Particle::Container> {
      // ======================================================================
      typedef LoKi::BasicFunctors<const LHCb::Particle*>::Source                                             _Source;
      typedef LoKi::Functors::Source<LHCb::Particle, LHCb::Particle::ConstVector, LHCb::Particle::Container> _Base;
      // ======================================================================
      static_assert( std::is_base_of<_Source, _Base>::value, "Invalid base" );
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from the service & TES location
      SourceTES( const IDataProviderSvc* svc, const std::string& path );
      /// constructor from the service, TES location and cuts
      SourceTES( const IDataProviderSvc* svc, const std::string& path, const LoKi::PhysTypes::Cuts& cuts );
      /// constructor from the algorithm & TES location
      SourceTES( const GaudiAlgorithm* svc, const std::string& path, const bool useRootInTES = true );
      /// constructor from the algorithm , TES location and cuts
      SourceTES( const GaudiAlgorithm* svc, const std::string& path, const LoKi::PhysTypes::Cuts& cuts,
                 const bool useRootInTES = true );
      /// MANDATORY: virtual destructor
      virtual ~SourceTES();
      /// MANDATORY: clone method ("virtual constructor")
      SourceTES* clone() const override;
      /// MANDATORY: the only essential method:
      result_type operator()() const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& o ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// get the cuts
      const LoKi::PhysTypes::Cuts& cut() const { return m_cut; }
      // ======================================================================
    public:
      // ======================================================================
      /// get the particles from TES
      StatusCode get( LHCb::Particle::ConstVector& output ) const;
      /// count  the particles in TES
      std::size_t count() const;
      // ======================================================================
    private:
      // ======================================================================
      /// 'on-flight' filter
      LoKi::PhysTypes::Cut m_cut;
      // ======================================================================
    };
    // ========================================================================
    /** @class SourceDesktop
     *  simple "source for the particles from desktop"
     *  @see LoKi::Cuts::SOURCEDESKTOP
     *  @see LoKi::Cuts::SOURCEDV
     *  @author Vanya BELYAEV ibelyav@physics.syr.edu
     *  @date 2006-12-07
     */
    class SourceDesktop : public LoKi::BasicFunctors<const LHCb::Particle*>::Source {
      // ======================================================================
      typedef LoKi::BasicFunctors<const LHCb::Particle*>::Source _Source;
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from the desktop and cuts
      SourceDesktop(
          const IDVAlgorithm*          desktop = 0,
          const LoKi::PhysTypes::Cuts& cuts    = LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// constructor from the desktop and cuts
      SourceDesktop( const LoKi::PhysTypes::Cuts& cuts, const IDVAlgorithm* desktop = 0 );
      /// copy constructor
      SourceDesktop( const SourceDesktop& right );
      /// MANDATORY: virtual destructor
      virtual ~SourceDesktop();
      /// MANDATORY: clone method ("virtual constructor")
      SourceDesktop* clone() const override { return new SourceDesktop( *this ); }
      /// MANDATORY: the only essential method:
      result_type operator()() const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& o ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// get the desktop
      const LoKi::Interface<IDVAlgorithm>& desktop() const { return m_desktop; }
      // ======================================================================
    public:
      // ======================================================================
      /// set the  desktop
      void setDesktop( const IDVAlgorithm* value ) { m_desktop = value; }
      /// set the  desktop
      void setDesktop( const LoKi::Interface<IDVAlgorithm>& value ) { m_desktop = value; }
      // ======================================================================
    public:
      // ======================================================================
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
      // ======================================================================
    private:
      // ======================================================================
      /// data provder service
      mutable LoKi::Interface<IDVAlgorithm> m_desktop; // data provder service
      /// 'on-flight' filter
      LoKi::PhysTypes::Cut m_cut; // 'on-flight' filter
      // ======================================================================
    };
    // ========================================================================
    /** @class TESData
     *  special source that relies on DataHandle to access data in TES
     *  @see LoKi::TES::DataHanble
     *  @see DataObjectReadHandle
     *  @see LoKi::Cuts::TESDATA
     *  @author Vanya BELYAEV Ivan.BElyaev@itep.ru
     *  @date    2018-08-20
     */
    class TESData : public LoKi::BasicFunctors<const LHCb::Particle*>::Source,
                    public LoKi::TES::DataHandle<Gaudi::Range_<LHCb::Particle::ConstVector>>
    // , public LoKi::TES::DataHandle<LHCb::Particle::Range>
    {
      typedef Gaudi::Range_<LHCb::Particle::ConstVector> Range_;
      // typedef LHCb::Particle::Range Range_ ;
    public:
      // ======================================================================
      /// constructor
      TESData( const GaudiAlgorithm* algorithm, const std::string& location );
      /// constructor with cuts
      TESData( const GaudiAlgorithm* algorithm, const std::string& location, const LoKi::PhysTypes::Cuts& cuts );
      /// MANDATORY: clone method ("virtual constructor")
      TESData* clone() const override;
      /// MANDATORY: the only essential method:
      result_type operator()() const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& o ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// 'on-flight' filter
      LoKi::PhysTypes::Cut m_cuts; // 'on-flight' filter
      // ======================================================================
    };
    // ========================================================================
    /** @class TESCounter
     *  simple functor to count good particles
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-25
     */
    class TESCounter : public LoKi::Functor<void, double> {
    public:
      // ======================================================================
      /// constructor from the source
      explicit TESCounter( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& s );
      // =============================================================================
      /// MANDATORY: clone method ("virtual constructor")
      TESCounter* clone() const override;
      /// MANDATORY: the only essential method:
      double operator()() const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& o ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the actual source
      LoKi::Assignable_t<LoKi::BasicFunctors<const LHCb::Particle*>::Source> m_source; // the actual source
      // ======================================================================
    };
    // ========================================================================
    /** @class Flatten
     *  @see LoKi::Cuts::FLATTEN
     *  simple functor to flatten the decay trees into plain list
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2012-01-18
     */
    class Flatten : public LoKi::BasicFunctors<const LHCb::Particle*>::Pipe {
    public:
      // ======================================================================
      /// constructor
      Flatten();
      /// constructor form the optional cuts
      Flatten( const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut );
      /// MANDATORY: virtual destructor
      virtual ~Flatten();
      /// MANDATORY: clone method("virtual constructor")
      Flatten* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& o ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// 'on-flight' filter
      LoKi::PhysTypes::Cut m_cut; // 'on-flight' filter
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
  namespace Vertices {
    // ========================================================================
    /** @class SourceTES
     *
     *  @see LoKi::Cuts::VSOURCE
     *  simple "source for the particles in TES"
     *  @author Vanya BELYAEV ibelyav@physics.syr.edu
     *  @date 2006-12-07
     */
    class SourceTES
        : public LoKi::Functors::Source<LHCb::VertexBase, LHCb::VertexBase::ConstVector, LHCb::RecVertex::Container> {
      // ======================================================================
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source _Source;
      typedef LoKi::Functors::Source<LHCb::VertexBase, LHCb::VertexBase::ConstVector, LHCb::RecVertex::Container> _Base;
      // ======================================================================
      static_assert( std::is_base_of<_Source, _Base>::value, "Invalid base" );
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from the service, TES location and cuts
      SourceTES( const IDataProviderSvc* svc, const std::string& path );
      SourceTES( const IDataProviderSvc* svc, const std::string& path, const LoKi::PhysTypes::VCuts& cuts );
      SourceTES( const GaudiAlgorithm* svc, const std::string& path, const bool useRootInTES = true );
      SourceTES( const GaudiAlgorithm* svc, const std::string& path, const LoKi::PhysTypes::VCuts& cuts,
                 const bool useRootInTES = true );
      /// MANDATORY: virtual destructor
      virtual ~SourceTES();
      /// MANDATORY: clone method ("virtual constructor")
      SourceTES* clone() const override;
      /// MANDATORY: the only essential method:
      result_type operator()() const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& o ) const override;
      // ======================================================================
    public:
      // ======================================================================
      const LoKi::PhysTypes::VCuts& cut() const { return m_cut; }
      // ======================================================================
    public:
      // ======================================================================
      /// get the vertiecs from the certain  TES location
      StatusCode get( LHCb::VertexBase::ConstVector& output ) const;
      // ======================================================================
      /// count the vertices from the certain  TES location
      std::size_t count() const;
      // ======================================================================
    private:
      // ======================================================================
      /// 'on-flight' filter
      LoKi::PhysTypes::VCut m_cut;
      // ======================================================================
    };
    // ========================================================================
    /** @class SourceDesktop
     *  @see LoKi::Cuts::VSOURCEDESKTOP
     *  simple "source for the vertices from desktop"
     *  @author Vanya BELYAEV ibelyav@physics.syr.edu
     *  @date 2006-12-07
     */
    class SourceDesktop : public LoKi::BasicFunctors<const LHCb::VertexBase*>::Source {
      // ======================================================================
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source _Source;

    public:
      // ======================================================================
      /// constructor from the desktop and cuts
      SourceDesktop(
          const IDVAlgorithm*           desktop,
          const LoKi::PhysTypes::VCuts& cuts = LoKi::BasicFunctors<const LHCb::VertexBase*>::BooleanConstant( true ) );
      /// constructor from the desktop and cuts
      SourceDesktop( const LoKi::PhysTypes::VCuts& cuts, const IDVAlgorithm* desktop = 0 );
      /// copy constructor
      SourceDesktop( const SourceDesktop& right );
      /// MANDATORY: virtual destructor
      virtual ~SourceDesktop();
      /// MANDATORY: clone method ("virtual constructor")
      SourceDesktop* clone() const override { return new SourceDesktop( *this ); }
      /// MANDATORY: the only essential method:
      result_type operator()() const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& o ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// get the desktop
      const LoKi::Interface<IDVAlgorithm>& desktop() const { return m_desktop; }
      // ======================================================================
    public:
      // ======================================================================
      /// set the  desktop
      void setDesktop( const IDVAlgorithm* value ) { m_desktop = value; }
      /// set the  desktop
      void setDesktop( const LoKi::Interface<IDVAlgorithm>& value ) { m_desktop = value; }
      // ======================================================================
    public:
      // ======================================================================
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
      // ======================================================================
    private:
      // ======================================================================
      /// data provder service
      mutable LoKi::Interface<IDVAlgorithm> m_desktop;
      /// 'on-flight' filter
      LoKi::PhysTypes::VCut m_cut;
      // ======================================================================
    };
    // ========================================================================
    /** @class TESData
     *  special source that relies on DataHandle to access data in TES
     *  @see LoKi::TES::DataHanble
     *  @see DataObjectReadHandle
     *  @see LoKi::Cuts::VTESDATA
     *  @author Vanya BELYAEV Ivan.BElyaev@itep.ru
     *  @date    2018-08-20
     */
    class TESData : public LoKi::BasicFunctors<const LHCb::VertexBase*>::Source,
                    public LoKi::TES::DataHandle<DataObject> {
    public:
      // ======================================================================
      /// constructor
      TESData( const GaudiAlgorithm* algorithm, const std::string& location );
      /// constructor with cuts
      TESData( const GaudiAlgorithm* algorithm, const std::string& location, const LoKi::PhysTypes::VCuts& cuts );
      /// MANDATORY: clone method ("virtual constructor")
      TESData* clone() const override;
      /// MANDATORY: the only essential method:
      result_type operator()() const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& o ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// 'on-flight' filter
      LoKi::PhysTypes::VCut m_cuts; // 'on-flight' filter
      // ======================================================================
    };
    // ========================================================================
    /** @class TESCounter
     *  simple functor to count good particles
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-25
     */
    class TESCounter : public LoKi::Functor<void, double> {
    public:
      // ======================================================================
      /// constructor from the source
      explicit TESCounter( const LoKi::BasicFunctors<const LHCb::VertexBase*>::Source& s );
      // =============================================================================
      /// MANDATORY: clone method ("virtual constructor")
      TESCounter* clone() const override;
      /// MANDATORY: the only essential method:
      double operator()() const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& o ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the actual source
      LoKi::Assignable_t<LoKi::BasicFunctors<const LHCb::VertexBase*>::Source> m_source; // the actual source
      // ======================================================================
    };
    // ========================================================================
  } // namespace Vertices
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef TESDATA
     *  simple "source"-functor to get the particle from TES
     *
     *  @code
     *  const SOURCE source = TESDATA ( "SomeLocationInTES" ) ;
     *  // get the particles:
     *  const LHCb::Particle::ConstVector result = source() ;
     *  @endcode
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2018-09-20
     */
    typedef LoKi::Particles::TESData TESDATA;
    // ========================================================================
    /** @typedef VTESDATA
     *  simple "source"-functor to get the vertices from TES
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2018-09-20
     */
    typedef LoKi::Vertices::TESData VTESDATA;
    // ========================================================================
    /** @typedef SOURCE
     *  simple "source"-functor to get the particle form TES
     *
     *  @code
     *
     *  const SOURCE source = SOURCE ( "SomeLocationInTES" , ALL ) ;
     *
     *  // get the particles:
     *  const LHCb::Particle::ConstVector result = source() ;
     *
     *  @endcode
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-29
     */
    typedef LoKi::Particles::SourceTES SOURCE;
    // ========================================================================
    /** @typedef SOURCETES
     *  simple "source"-functor to get the particle form TES
     *
     *  @code
     *
     *  const SOURCETES source = SOURCETES ( "SomeLocationInTES" , ALL ) ;
     *
     *  // get the particles:
     *  const LHCb::Particle::ConstVector result = source() ;
     *
     *  @endcode
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-29
     */
    typedef LoKi::Particles::SourceTES SOURCETES;
    // ========================================================================
    /** @typedef SOURCEDESKTOP
     *  Somiple "source"-functor which gets the particles from
     *  desktop
     *
     *  @code
     *
     *  const SOURCEDESKTOP source ( "pi+" == ABSID ) ;
     *
     *  // get the pions from desktop:
     *  const LHCb::Particle::ConstVector result = source() ;
     *
     *  @endcode
     *
     *  @see IPhysDekstop
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-29
     */
    typedef LoKi::Particles::SourceDesktop SOURCEDESKTOP;
    // ========================================================================
    /** @typedef SOURCEDV
     *  Somiple "source"-functor which gets the particles from
     *  desktop
     *
     *  @code
     *
     *  const SOURCEDV source ( "pi+" == ABSID ) ;
     *
     *  // get the pions from desktop:
     *  const LHCb::Particle::ConstVector result = source() ;
     *
     *  @endcode
     *
     *  @see IPhysDekstop
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-29
     */
    typedef LoKi::Particles::SourceDesktop SOURCEDV;
    // ========================================================================
    /** @typedef FLATTEN
     *  Flatten the decay trees into plain list (with optional selection)
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2012-01-18
     */
    typedef LoKi::Particles::Flatten FLATTEN;
    // ========================================================================
    /** @typedef VSOURCE
     *  simple "source"-functor to get the vertices from TES
     *
     *  @code
     *
     *  const VSOURCE source = VSOURCE ( "SomeLocationInTES" , PRIMARY ) ;
     *
     *  // get the vertices
     *  const LHCb::VertexBase::ConstVector result = source() ;
     *
     *  @endcode
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-29
     */
    typedef LoKi::Vertices::SourceTES VSOURCE;
    // ========================================================================
    /** @typedef VSOURCETES
     *  simple "source"-functor to get the vertices from TES
     *
     *  @code
     *
     *  const VSOURCETES source ( "SomeLocationInTES" , PRIMARY ) ;
     *
     *  // get the primary vertices:
     *  const LHCb::VertexBase::ConstVector result = source() ;
     *
     *  @endcode
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-29
     */
    typedef LoKi::Vertices::SourceTES VSOURCETES;
    // ========================================================================
    /** @typedef VSOURCEDESKTOP
     *  Somiple "source"-functor which gets the vertices from
     *  desktop
     *
     *  @code
     *
     *  const VSOURCEDESKTOP source ( PRIMARY  ) ;
     *
     *  // get the primary vertices from desktop:
     *  const LHCb::VertexBase::ConstVector result = source() ;
     *
     *  @endcode
     *
     *  @see IPhysDekstop
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-29
     */
    typedef LoKi::Vertices::SourceDesktop VSOURCEDESKTOP;
    // ========================================================================
    /** @typedef VSOURCEDV
     *  Somiple "source"-functor which gets the vertices from
     *  desktop
     *
     *  @code
     *
     *  const VSOURCEDV source ( PRIMARY ) ;
     *
     *  // get the primiaries from desktop:
     *  const LHCb::VertexBase::ConstVector result = source() ;
     *
     *  @endcode
     *
     *  @see IPhysDekstop
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-29
     */
    typedef LoKi::Vertices::SourceDesktop VSOURCEDV;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_PHYSSOURCES_H
// ============================================================================
