/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES34_H
#  define LOKI_PARTICLES34_H 1
// ============================================================================
// Include files
// ============================================================================
// DaVinciInterfaces
// ============================================================================
#  include "Kernel/ILifetimeFitter.h"
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/PhysTypes.h"
// ============================================================================
/** @file
 *  Set of functions for Kazu Karvalho Akiba
 *
 *  Moved to file LoKi/Legacy.h
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2010-05-15
 *
 */
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES34_H
// ============================================================================
