/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <memory>
#include <ostream>
#include <vector>

#include "Kernel/IParticleDictTool.h"
#include "TMVA/Reader.h"

#include "MVADictTools/Options.h"

/** @class TMVATransform
 *  Policy class to be used by the DictTransform template
 *  Implementing the TMVA Reader backend
 *
 *  @author Sam Hall
 *  @date   2013-07-29
 */

class TMVATransform {

public:
  using optmap = std::map<std::string, std::string>;

private:
  bool        m_setup_success{false};
  bool        m_keep_all_vars{false};
  std::string m_reader_opts{"Silent"}; // Silent : none,  V : verbose, Color : colored
  std::string m_weightfile;
  std::string m_name;
  std::string m_branchname;
  std::string m_default_path;

  // TMVA bits
  std::unique_ptr<TMVA::Reader> m_reader;

  // spectators variables.
  std::vector<std::string>          m_spectator;
  std::vector<std::unique_ptr<int>> m_spectator_vars;

  bool m_debug{false};

private:
  // Helper Functions
  void readWeightsFile( std::ostream& );
  void setupReader( std::ostream& );
  bool parseOpts( const optmap&, std::ostream& );

public:
  std::vector<std::string> m_variables; /// variables needed by the classifier

public:
  // the policy methods neede for collaboration with DictTransform
  bool Init( const optmap& options, std::ostream& info, const bool debug = false );
  bool operator()( const IParticleDictTool::DICT& in, IParticleDictTool::DICT& out ) const;
  bool checkWeightsFile( std::ostream& info );
};
