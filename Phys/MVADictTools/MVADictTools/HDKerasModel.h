/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LIB_HDKERASMODEL_H
#define LIB_HDKERASMODEL_H

#include <string>

#include "keras/keras_model.h"
#include "lwtnnParser/parse_json.h"

class HDKerasLayer : public KerasLayer {
public:
  using KerasLayer::Apply;
  using KerasLayer::LoadLayer;

  /// Interface to load layer from JSON configuration object
  virtual bool LoadLayer( const lwt::LayerConfig& config, size_t n_inputs = 0 ) = 0;

  /// Read flattened transpose of matrix to 2D tensor
  virtual Tensor getMatrixTensor( const lwt::LayerConfig& config, size_t n_inputs );

  /// Get number of outputs for current layer
  virtual size_t GetNOutputs() { return m_n_outputs; }

protected:
  size_t m_n_outputs = 0; ///< store number of outputs for current layer
};

class HDKerasLayerActivation : public KerasLayerActivation, public HDKerasLayer {
public:
  virtual bool LoadLayer( std::ifstream* file ) override {
    return KerasLayerActivation::LoadLayer( file );
  } ///< call internal inherited
  virtual bool Apply( Tensor* in, Tensor* out ) override {
    return KerasLayerActivation::Apply( in, out );
  } ///< call internal inherited

  /// Interface to load layer from JSON configuration object
  virtual bool LoadLayer( const lwt::LayerConfig& config, size_t n_inputs = 0 ) override;
};

class HDKerasLayerDense : public KerasLayerDense, public HDKerasLayer {
public:
  virtual bool LoadLayer( std::ifstream* file ) override {
    return KerasLayerDense::LoadLayer( file );
  } ///< call internal inherited
  virtual bool Apply( Tensor* in, Tensor* out ) override {
    return KerasLayerDense::Apply( in, out );
  } ///< call internal inherited

  /// Interface to load layer from JSON configuration object
  virtual bool LoadLayer( const lwt::LayerConfig& config, size_t n_inputs = 0 ) override;

protected:
  HDKerasLayerActivation activation_;
};

class HDKerasLayerConvolution2d : public KerasLayerConvolution2d, public HDKerasLayer {
public:
  virtual bool LoadLayer( std::ifstream* file ) override {
    return KerasLayerConvolution2d::LoadLayer( file );
  } ///< call internal inherited
  virtual bool Apply( Tensor* in, Tensor* out ) override {
    return KerasLayerConvolution2d::Apply( in, out );
  } ///< call internal inherited

  /// Interface to load layer from JSON configuration object
  virtual bool LoadLayer( const lwt::LayerConfig& config, size_t n_inputs = 0 ) override;

protected:
  HDKerasLayerActivation activation_;
};

class HDKerasLayerFlatten : public KerasLayerFlatten, public HDKerasLayer {
public:
  virtual bool LoadLayer( std::ifstream* file ) override {
    return KerasLayerFlatten::LoadLayer( file );
  } ///< call internal inherited
  virtual bool Apply( Tensor* in, Tensor* out ) override {
    return KerasLayerFlatten::Apply( in, out );
  } ///< call internal inherited

  /// Interface to load layer from JSON configuration object
  virtual bool LoadLayer( const lwt::LayerConfig& config, size_t n_inputs = 0 ) override;
};

class HDKerasLayerElu : public KerasLayerElu, public HDKerasLayer {
public:
  virtual bool LoadLayer( std::ifstream* file ) override {
    return KerasLayerElu::LoadLayer( file );
  } ///< call internal inherited
  virtual bool Apply( Tensor* in, Tensor* out ) override {
    return KerasLayerElu::Apply( in, out );
  } ///< call internal inherited

  /// Interface to load layer from JSON configuration object
  virtual bool LoadLayer( const lwt::LayerConfig& config, size_t n_inputs = 0 ) override;
};

class HDKerasLayerMaxPooling2d : public KerasLayerMaxPooling2d, public HDKerasLayer {
public:
  virtual bool LoadLayer( std::ifstream* file ) override {
    return KerasLayerMaxPooling2d::LoadLayer( file );
  } ///< call internal inherited
  virtual bool Apply( Tensor* in, Tensor* out ) override {
    return KerasLayerMaxPooling2d::Apply( in, out );
  } ///< call internal inherited

  /// Interface to load layer from JSON configuration object
  virtual bool LoadLayer( const lwt::LayerConfig& config, size_t n_inputs = 0 ) override;
};

class HDKerasLayerLSTM : public KerasLayerLSTM, public HDKerasLayer {
public:
  virtual bool LoadLayer( std::ifstream* file ) override {
    return KerasLayerLSTM::LoadLayer( file );
  } ///< call internal inherited
  virtual bool Apply( Tensor* in, Tensor* out ) override {
    return KerasLayerLSTM::Apply( in, out );
  } ///< call internal inherited

  /// Interface to load layer from JSON configuration object
  virtual bool LoadLayer( const lwt::LayerConfig& config, size_t n_inputs = 0 ) override;

protected:
  HDKerasLayerActivation innerActivation_;
  HDKerasLayerActivation activation_;
};

class HDKerasLayerEmbedding : public KerasLayerEmbedding, public HDKerasLayer {
public:
  virtual bool LoadLayer( std::ifstream* file ) override {
    return KerasLayerEmbedding::LoadLayer( file );
  } ///< call internal inherited
  virtual bool Apply( Tensor* in, Tensor* out ) override {
    return KerasLayerEmbedding::Apply( in, out );
  } ///< call internal inherited

  /// Interface to load layer from JSON configuration object
  virtual bool LoadLayer( const lwt::LayerConfig& config, size_t n_inputs = 0 ) override;
};

class HDKerasLayerBatchNormalization : public KerasLayerBatchNormalization, public HDKerasLayer {
public:
  virtual bool LoadLayer( std::ifstream* file ) override {
    return KerasLayerBatchNormalization::LoadLayer( file );
  } ///< call internal inherited
  virtual bool Apply( Tensor* in, Tensor* out ) override {
    return KerasLayerBatchNormalization::Apply( in, out );
  } ///< call internal inherited

  /// Interface to load layer from JSON configuration object
  virtual bool LoadLayer( const lwt::LayerConfig& config, size_t n_inputs = 0 ) override;
};

class HDKerasModel : public KerasModel {
public:
  using KerasModel::LoadModel;

  /// Interface to load Keras Neural Network from JSON configuration file
  virtual bool LoadJSONModel( const std::string& filename );

  /// Interface to load Keras Neural Network from JSON configuration object
  virtual bool LoadJSONModel( const lwt::JSONConfig& config );
};

#endif // LIB_HDKERASMODEL_H
