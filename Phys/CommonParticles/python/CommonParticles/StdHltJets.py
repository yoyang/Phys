#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file for 'StdHltJets'. Reconstructed jets produced by the HltJets algorithm.
"""
__author__ = "Carlos Vazquez Sierra"
__date__ = "25/04/2018"
__version__ = "v1r1"

# =============================================================================
__all__ = ('StdHltJets', 'locations')
# =============================================================================
from Gaudi.Configuration import *
from CommonParticles.Utils import *

from Configurables import HltJetBuilder, HltParticleFlow, GaudiSequencer
from JetAccessories.HltJetConf import HltJetBuilderConf, HltParticleFlowConf
from GaudiKernel.SystemOfUnits import GeV

_defaultInputs = [
    'Photons', 'ResolvedPi0s', 'MergedPi0s', 'Ks', 'Lambda', 'ChargedProtos',
    'VeloProtos', 'EcalClusters', 'HcalClusters', 'EcalMatch', 'HcalMatch',
    'PrimaryVtxs'
]

ParticleFlow = HltParticleFlowConf(
    'StdHltPF', SprRecover=True, _inputs=_defaultInputs
)  # output location is Phys/StdHltParticleFlowSeq/Particles
JetBuilder = HltJetBuilderConf(
    'StdHltJets',
    ParticleFlow.getOutputLocation(),
    JetEcPath='',
    JetPtMin=5 * GeV,
    JetVrt=False)

algorithm = GaudiSequencer('StdHltJetsSeq')
algorithm.Members += [ParticleFlow.getSeq()]
algorithm.Members += [JetBuilder.getSeq()]

## configure Data-On-Demand service
locations = updateDoD(algorithm, name='StdHltJets')

## finally: define the symbol
StdHltJets = algorithm

## ============================================================================
if '__main__' == __name__:

    print __doc__
    print __author__
    print __version__
    print locationsDoD(locations)

# =============================================================================
# The END
# =============================================================================
