###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: JetAccessories
################################################################################
gaudi_subdir(JetAccessories)

gaudi_depends_on_subdirs(Calo/CaloUtils
                         Kernel/LHCbMath
                         Phys/LoKiAlgo
                         Phys/LoKiJets)

find_package(FastJet)

gaudi_install_headers(Kernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(JetAccessories
                 src/*.cpp
                 INCLUDE_DIRS FastJet
                 LINK_LIBRARIES FastJet CaloUtils LHCbMathLib LoKiAlgo)

gaudi_install_python_modules()

gaudi_add_dictionary(JetAccessories
                     dict/JetAccessoriesDict.h
                     dict/JetAccessories.xml
                     LINK_LIBRARIES FastJet CaloUtils LHCbMathLib LoKiAlgo
                     OPTIONS "-U__MINGW32__")
