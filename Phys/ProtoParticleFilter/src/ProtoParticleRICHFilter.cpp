/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ProtoParticleRICHFilter.cpp
 *
 * Implementation file for algorithm ProtoParticleRICHFilter
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2006-05-03
 */
//-----------------------------------------------------------------------------

// local
#include "ProtoParticleRICHFilter.h"

// namespaces
using namespace LHCb;

//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( ProtoParticleRICHFilter )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ProtoParticleRICHFilter::ProtoParticleRICHFilter( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
    : ChargedProtoParticleDLLFilter( type, name, parent ) {}

//=============================================================================
// Destructor
//=============================================================================
ProtoParticleRICHFilter::~ProtoParticleRICHFilter() {}

//=============================================================================
// Create a cut object from decoded cut options
//=============================================================================
const ProtoParticleSelection::Cut* ProtoParticleRICHFilter::createCut( const std::string& tag, const std::string& delim,
                                                                       const std::string& value ) const {
  // Attempt to create a cut object using base class method first
  const ProtoParticleSelection::Cut* basecut = ChargedProtoParticleDLLFilter::createCut( tag, delim, value );

  // If a non-null pointer is returned, base class was able to decode the data, so return
  if ( basecut ) return basecut;
  // Otherwise, cut data is RICH specific, so treat here

  // Try to get a double from the value
  double cut_value = 0;
  if ( !stringToDouble( value, cut_value ) ) return NULL;

  // Create a new Cut object, of type DLLCut
  ProtoParticleSelection::DLLCut* dllcut = new ProtoParticleSelection::DLLCut();

  // set cut properties
  // Cut delimiter type
  dllcut->setDelim( ProtoParticleSelection::Cut::delimiter( delim ) );
  // cut value
  dllcut->setCutValue( cut_value );
  // cut description
  dllcut->setDescription( "Rich DLL : " + tag + " " + delim + " " + value );

  // no obvious way to avoid a big bunch of ifs for the dlls ???
  if ( "RICHDLL(E-PI)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLe, LHCb::ProtoParticle::additionalInfo::RichDLLpi );
  } else if ( "RICHDLL(E-MU)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLe, LHCb::ProtoParticle::additionalInfo::RichDLLmu );
  } else if ( "RICHDLL(E-K)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLe, LHCb::ProtoParticle::additionalInfo::RichDLLk );
  } else if ( "RICHDLL(E-P)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLe, LHCb::ProtoParticle::additionalInfo::RichDLLp );
  } else if ( "RICHDLL(MU-PI)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLmu, LHCb::ProtoParticle::additionalInfo::RichDLLpi );
  } else if ( "RICHDLL(MU-E)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLmu, LHCb::ProtoParticle::additionalInfo::RichDLLe );
  } else if ( "RICHDLL(MU-K)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLmu, LHCb::ProtoParticle::additionalInfo::RichDLLk );
  } else if ( "RICHDLL(MU-P)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLmu, LHCb::ProtoParticle::additionalInfo::RichDLLp );
  } else if ( "RICHDLL(K-PI)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLk, LHCb::ProtoParticle::additionalInfo::RichDLLpi );
  } else if ( "RICHDLL(K-MU)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLk, LHCb::ProtoParticle::additionalInfo::RichDLLmu );
  } else if ( "RICHDLL(K-E)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLk, LHCb::ProtoParticle::additionalInfo::RichDLLe );
  } else if ( "RICHDLL(K-P)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLk, LHCb::ProtoParticle::additionalInfo::RichDLLp );
  } else if ( "RICHDLL(P-PI)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLp, LHCb::ProtoParticle::additionalInfo::RichDLLpi );
  } else if ( "RICHDLL(P-MU)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLp, LHCb::ProtoParticle::additionalInfo::RichDLLmu );
  } else if ( "RICHDLL(P-K)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLp, LHCb::ProtoParticle::additionalInfo::RichDLLk );
  } else if ( "RICHDLL(PI-E)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLpi, LHCb::ProtoParticle::additionalInfo::RichDLLe );
  } else if ( "RICHDLL(PI-MU)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLpi, LHCb::ProtoParticle::additionalInfo::RichDLLmu );
  } else if ( "RICHDLL(PI-K)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLpi, LHCb::ProtoParticle::additionalInfo::RichDLLk );
  } else if ( "RICHDLL(PI-P)" == tag ) {
    dllcut->setDLLs( LHCb::ProtoParticle::additionalInfo::RichDLLpi, LHCb::ProtoParticle::additionalInfo::RichDLLp );
  } else {
    debug() << "Unknown DLL tag " << tag << endmsg;
    delete dllcut;
    dllcut = NULL;
  }

  if ( msgLevel( MSG::DEBUG ) && dllcut ) {
    debug() << "  -> Created new DLLCut : " << tag << " " << delim << " " << dllcut->cutValue() << endmsg;
  }

  return dllcut;
}

//=============================================================================
