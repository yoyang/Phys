/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <assert.h>

#include <functional>

#include <boost/algorithm/string/join.hpp>

#include "LoKi/Combiner.h"
#include "LoKi/VertexCuts.h"
#include "TrackKernel/TrackStateVertex.h"
#include "TrackKernel/TrackVertexUtils.h"

#include "TrackCombiner.h"

/// Vertex fitters used by the class
namespace {
  /// Fit an N-body vertex using TrackStateVertex
  void fit_vertex_n_body( LHCb::RecVertex& vertex ) {
    auto const&                     tracks = vertex.tracks();
    std::vector<const LHCb::State*> states;
    for ( auto const& track : tracks ) {
      auto state = track->stateAt( LHCb::State::ClosestToBeam );
      assert( state != nullptr );
      states.push_back( state );
    }

    LHCb::TrackStateVertex fitter( states );

    // Fill the vertex with the fit result
    vertex.setCovMatrix( fitter.covMatrix() );
    vertex.setPosition( fitter.position() );
    vertex.setChi2AndDoF( fitter.chi2(), fitter.nDoF() );
  }
} // namespace

DECLARE_COMPONENT( TrackCombiner )

TrackCombiner::TrackCombiner( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"InputTracks", LHCb::TrackLocation::Default}},
                   {KeyValue{"OutputVertices", ""}} )
    , m_combination_cut( LoKi::Cuts::VFALSE )
    , m_vertex_cut( LoKi::Cuts::VFALSE ) {}

StatusCode TrackCombiner::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) { return sc; }

  // Can't fit a one-body vertex
  if ( m_nbodies < 2 ) { return Error( "Cannot fit a N-body vertex with N < 2" ); }

  // Form the preamble by concatenating the list items with newlines
  m_preamble = boost::algorithm::join( m_preamble_def.value(), "\n" );

  // Initialise the pre-fit selection object
  sc = m_factory->get( m_combination_cut_def, m_combination_cut, m_preamble );
  if ( sc.isFailure() ) { return sc; }

  // Initialise the post-fit selection object
  sc = m_factory->get( m_vertex_cut_def, m_vertex_cut, m_preamble );
  if ( sc.isFailure() ) { return sc; }

  return StatusCode::SUCCESS;
}

std::vector<LHCb::RecVertex> TrackCombiner::operator()( Pr::Selection<LHCb::Event::v1::Track> const& tracks ) const {
  using Tracks        = std::vector<const LHCb::Event::v1::Track*>;
  using Combiner      = LoKi::Combiner_<Tracks>;
  using CombinerRange = typename Combiner::Range;

  // Create vector of pointers from vector of objects,
  // as needed for the combinatorics engine
  // TODO This is a hack; the combinatorics engine should accept Pr::Selection
  Tracks ptracks;
  ptracks.reserve( tracks.size() );
  for ( auto const& track : tracks ) { ptracks.push_back( &track ); }

  // Create the combinatorics iterator
  Combiner      combiner;
  CombinerRange range( ptracks.begin(), ptracks.end() );
  for ( int i = 0; i < m_nbodies; ++i ) { combiner.add( range ); }

  // Create vertices from each combination
  std::vector<LHCb::RecVertex> vertices;
  auto                         ncombinations = 0;
  // A comparator to impose pair-wise order within each combination
  std::less<> comparator;
  for ( ; combiner.valid(); combiner.next() ) {
    // Filter out combinations that are not ordered
    // Because only one permutation of a given set of elements will satisfy the
    // ordering criteria, we will skip all other permutations (which is good,
    // because they would result in the same vertex)
    // This criteria also means we skip combinations with duplicate elements
    if ( !combiner.unique( comparator ) ) { continue; }

    // Get the current combination
    Tracks combination( combiner.dim() );
    combiner.current( combination.begin() );
    ++ncombinations;

    // Create an (effectively uninitialised) vertex filled with the tracks
    LHCb::RecVertex vertex;
    for ( auto const* track : combination ) { vertex.addToTracks( track ); }

    // Apply the pre-fit selection
    auto passed_combination = m_combination_cut( &vertex );
    m_npassed_combination_cut += passed_combination;
    if ( !passed_combination ) { continue; }

    // Perform the fit
    fit_vertex_n_body( vertex );
    auto fit_chi2   = vertex.chi2();
    auto passed_fit = fit_chi2 > 0;
    m_npassed_vertex_fit += passed_fit;
    if ( !passed_fit ) { continue; }

    // Apply the post-fit selection
    auto passed_vertex = m_vertex_cut( &vertex );
    m_npassed_vertex_cut += passed_vertex;
    if ( !passed_vertex ) { continue; }

    vertices.push_back( std::move( vertex ) );
  }

  m_ncombinations += ncombinations;

  return vertices;
}
