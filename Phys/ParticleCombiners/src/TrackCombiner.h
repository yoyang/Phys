/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "GaudiAlg/Transformer.h"

#include "Event/RecVertex.h"
#include "Event/Track_v1.h"
#include "LoKi/IHybridFactory.h"
#include "PrKernel/PrSelection.h"

/** @class TrackCombiner TrackCombiner.h
 *
 * @brief N-body combiner for LHCb::Event::v1::Track objects.
 *
 * All input Track objects will be taken from a single input container. For
 * each combination of Track objects, a vertex is fitted from the set of
 * LHCb::State::ClosestToBeam states.
 *
 * @todo Consider adding an overlap checking tool, e.g. LoKi::CheckOverlap, for
 * usage before the combination cut
 * @todo Reserve the output vertex vector once we have an understanding of the
 * number of vertices a typical selection creates
 * @todo The comparator used for ensuring unique track object permutations does
 * not guarantee ordering from one application run to the next (as it uses
 * pointer values), neither for the Track vector inserted into the vertex nor
 * the vertex creation order itself. Should consider ordering these for
 * reproducibility (ideally after as much selection as possible).
 * @todo The RecVertex creation/destruction, one per combination, adds up to a
 * huge overall cost (30-40%). Should consider avoiding this, e.g. optimising
 * RecVertex or using a (as-yet non-existent) family of ATr functors that can
 * act on Track vectors directly.
 *
 * Example usage:
 *
 * @code {.py}
 * combiner = TrackCombiner("TwoBodyCombiner")
 * combiner.NBodies = 2
 * combiner.Preamble = [
 *     "from GaudiKernel.SystemOfUnits import MeV, mm",
 *     "from LoKiPhys.decorators import RV_MASS, VCHI2PDOF, VZ"
 * ]
 * combiner.CombinationCut = "RV_MASS('mu-', 'mu+') < 1000*MeV"
 * combiner.VertexCut = "(VCHI2PDOF < 8) & (VZ > -100*mm)"
 * @endcode
 *
 * @see CombineParticles Similar class for LHCb::Particle objects
 *
 */
class TrackCombiner : public Gaudi::Functional::Transformer<std::vector<LHCb::RecVertex>(
                          Pr::Selection<LHCb::Event::v1::Track> const& )> {

public:
  TrackCombiner( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  std::vector<LHCb::RecVertex> operator()( Pr::Selection<LHCb::Event::v1::Track> const& tracks ) const override;

private:
  ToolHandle<LoKi::IHybridFactory> m_factory = {"LoKi::Hybrid::Tool/HybridFactory:PUBLIC", this};

  Gaudi::Property<int> m_nbodies{this, "NBodies", 2, "Number of tracks to include in each combination"};
  Gaudi::Property<std::vector<std::string>> m_preamble_def{this, "Preamble", {}, "List of preamble strings"};
  Gaudi::Property<std::string>              m_combination_cut_def{this, "CombinationCut", "DEFINE_ME",
                                                     "Cut string applied to the N-body combination"};
  Gaudi::Property<std::string>              m_vertex_cut_def{this, "VertexCut", "DEFINE_ME",
                                                "Cut string applied to the fitted vertex"};

  /// Newline-delimited preamble
  std::string m_preamble;

  /// Cut object for the pre-fit combination selection
  LoKi::Types::VCut m_combination_cut;
  /// Cut object for the fitted vertex selection
  LoKi::Types::VCut m_vertex_cut;

  /// Number of N-body Track combinations created
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_ncombinations{this, "Nb created combinations"};
  /// Number of combinations passing the combination cut
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_combination_cut{this, "# passed combination cut"};
  /// Number of successfully fitted combinations
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_vertex_fit{this, "# passed vertex fit"};
  /// Number of fitted vertices passing vertex cut
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_vertex_cut{this, "# passed vertex cut"};
};
