###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: DecayTreeFitter
################################################################################
gaudi_subdir(DecayTreeFitter)

gaudi_depends_on_subdirs(Phys/DaVinciKernel
                         Phys/LoKiCore
                         Tr/TrackInterfaces
                         Tr/TrackKernel)

find_package(CLHEP)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(DecayTreeFitter
                  src/*.cpp
                  PUBLIC_HEADERS DecayTreeFitter
                  INCLUDE_DIRS CLHEP AIDA CLHEP Tr/TrackInterfaces
                  LINK_LIBRARIES CLHEP CLHEP DaVinciKernelLib LoKiCoreLib TrackKernel)

gaudi_add_dictionary(DecayTreeFitter
                     dict/DecayTreeFitterDict.h
                     dict/DecayTreeFitterDict.xml
                     INCLUDE_DIRS CLHEP Tr/TrackInterfaces
                     LINK_LIBRARIES CLHEP DaVinciKernelLib LoKiCoreLib TrackKernel DecayTreeFitter
                     OPTIONS "-U__MINGW32__")

