###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: LoKiProtoParticles
################################################################################
gaudi_subdir(LoKiProtoParticles)

gaudi_depends_on_subdirs(Event/RecEvent
                         Phys/DaVinciInterfaces
                         Phys/LoKiCore
                         Phys/LoKiUtils
                         Phys/LoKiTracks)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(LoKiProtoParticles
                  src/*.cpp
                  PUBLIC_HEADERS LoKi
                  LINK_LIBRARIES RecEvent DaVinciInterfacesLib LoKiCoreLib LoKiUtils LoKiTracksLib)

gaudi_add_dictionary(LoKiProtoParticles
                     dict/LoKiProtoParticlesDict.h
                     dict/LoKiProtoParticles.xml
                     LINK_LIBRARIES RecEvent DaVinciInterfacesLib LoKiCoreLib LoKiUtils LoKiTracksLib LoKiProtoParticles
                     OPTIONS " -U__MINGW32__ ")

gaudi_install_python_modules()


gaudi_add_test(QMTest QMTEST)
