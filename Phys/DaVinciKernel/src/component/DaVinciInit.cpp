/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// from Gaudi
#include "GaudiAlg/IGenericTool.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/Incident.h"
#include "GaudiKernel/Memory.h"

// Event model
#include "Event/ODIN.h"
#include "Event/Particle.h"
#include "Event/ProcStatus.h"
#include "Event/ProcessHeader.h"
#include "Event/ProtoParticle.h"
#include "Event/RecHeader.h"
#include "Event/Track.h"
#include "Event/Vertex.h"

// local
#include "DaVinciInit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DaVinciInit
//
// 2008-03-02 : P. Koppenburg
//-----------------------------------------------------------------------------

//=============================================================================

DaVinciInit::DaVinciInit( const std::string& name, ISvcLocator* pSvcLocator ) : LbAppInit( name, pSvcLocator ) {
  declareProperty( "PrintEvent", m_print = false, "Print Event and Run Number" );
}

//=============================================================================

StatusCode DaVinciInit::initialize() {
  const StatusCode sc = LbAppInit::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;               // error printed already by GaudiAlgorithm

  // Private tool to plot the memory usage
  m_memoryTool = tool<IGenericTool>( "MemoryTool", "DaVinciMemory", this, true );

  return sc;
}

//=============================================================================

StatusCode DaVinciInit::execute() {
  // count events
  increaseEventCounter();

  // memory check
  checkMem();

  // Plot the memory usage
  m_memoryTool->execute();

  // Get the run and event number from the ODIN bank
  if ( m_print ) {
    const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>( LHCb::ODINLocation::Default );
    if ( odin ) {
      this->printEventRun( odin->eventNumber(), odin->runNumber() );
    } else {
      Warning( "No ODIN bank", StatusCode::SUCCESS, 1 ).ignore();
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DaVinciInit )

//=============================================================================
