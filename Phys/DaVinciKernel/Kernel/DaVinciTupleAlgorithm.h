/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef KERNEL_DaVinciTupleAlgorithm_H
#define KERNEL_DaVinciTupleAlgorithm_H 1

// Local base class
#include "Kernel/DVCommonBase.h"

// Gaudi base class
#include "GaudiAlg/GaudiTupleAlg.h"

/** @class DaVinciAlgorithm Kernel/DaVinciAlgorithm.h
 *
 *  DaVinci Algorithm with histogramming and tupling
 *
 *  @author Chris Jones
 *  @date   2012-06-22
 */
class DaVinciTupleAlgorithm : public DVCommonBase<GaudiTupleAlg> {

public:
  /// Standard constructor
  DaVinciTupleAlgorithm( const std::string& name, ISvcLocator* pSvcLocator )
      : DVCommonBase<GaudiTupleAlg>( name, pSvcLocator ) {}
};

#endif // KERNEL_DaVinciTupleAlgorithm_H
