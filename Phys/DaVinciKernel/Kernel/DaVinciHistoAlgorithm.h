/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef KERNEL_DaVinciHistoAlgorithm_H
#define KERNEL_DaVinciHistoAlgorithm_H 1

// Local base class
#include "Kernel/DVCommonBase.h"

// Gaudi base class
#include "GaudiAlg/GaudiHistoAlg.h"

/** @class DaVinciHistoAlgorithm Kernel/DaVinciHistoAlgorithm.h
 *
 *  DaVinci Algorithm with histogramming
 *
 *  @author Chris Jones
 *  @date   2012-06-22
 */
class DaVinciHistoAlgorithm : public DVCommonBase<GaudiHistoAlg> {

public:
  /// Standard constructor
  DaVinciHistoAlgorithm( const std::string& name, ISvcLocator* pSvcLocator )
      : DVCommonBase<GaudiHistoAlg>( name, pSvcLocator ) {}
};

#endif // KERNEL_DaVinciHistoAlgorithm_H
