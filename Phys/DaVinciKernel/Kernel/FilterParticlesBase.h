/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FilterParticlesBase.h,v 1.1 2007-07-20 14:55:13 jpalac Exp $
#ifndef KERNEL_FILTERPARTICLESBASE_H
#define KERNEL_FILTERPARTICLESBASE_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IFilterParticles.h" // Interface

/** @class FilterParticlesBase FilterParticlesBase.h Kernel/FilterParticlesBase.h
 *
 *
 *  @author Juan Palacios
 *  @date   2007-07-20
 */
class FilterParticlesBase : public GaudiTool, virtual public IFilterParticles {

public:
  /// Standard constructor
  FilterParticlesBase( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~FilterParticlesBase(); ///< Destructor

  /// Test if filter is satisfied on ensemble of particles
  bool isSatisfied( const LHCb::Particle::ConstVector& ) const override;

  /// Test if filter is satisfied on ensemble of particles
  virtual bool operator()( const LHCb::Particle::ConstVector& ) const;
};

#endif // KERNEL_FILTERPARTICLESBASE_H
