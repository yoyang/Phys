#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#$Id: test_selection_components.py,v 1.2 2010/02/25 14:13:33 jpalac Exp $
'''
Test suite for selection module.
'''

__author__ = "Juan PALACIOS juan.palacios@nikhef.nl"

from nose.tools import raises
from SelPy.configurabloids import DummyAlgorithm
from SelPy.selection import AutomaticData, SelectionSequence, NameError, SelectionBase

alg = AutomaticData(Location='Test/SelSeq/Location')


@raises(NameError)
def test_SelectionSequence_duplicate_name_raises():
    ss = SelectionSequence('SelSeqUniqueNameTest', TopSelection=alg)
    SelectionSequence('SelSeqUniqueNameTest', TopSelection=alg)


def test_SelectionSequence_name():
    sel = SelectionSequence('SelSeqNameTest', TopSelection=alg)
    assert sel.name() == 'SelSeqNameTest'


def test_SelectionSelection_outputLocaiton():
    sel = SelectionSequence('SelSeqOutputTest', TopSelection=alg)
    assert sel.outputLocation() == alg.outputLocation()


def test_SelectionSequence_NoneTypes_not_in_algorithms():
    sel = SelectionBase(
        algorithm=None, outputLocation='TestLocation', requiredSelections=[])
    ss = SelectionSequence('SelSeqTestNoneTypes', TopSelection=sel)
    assert None not in ss.algorithms()


if __name__ == '__main__':
    import nose
    from os.path import basename, splitext
    nose.run(defaultTest=splitext(basename(__file__))[0])
