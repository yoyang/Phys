###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: JetTagging
################################################################################
gaudi_subdir(JetTagging)

gaudi_depends_on_subdirs(Phys/LoKiJets
                         Phys/LoKiAlgo
                         Kernel/LHCbMath
                         Phys/JetAccessories
                         Phys/DaVinciInterfaces
                         )

find_package(NeuroBayesExpert)
find_package(ROOT COMPONENTS TMVA Physics)

gaudi_install_headers(Kernel)

find_package(Boost)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

if(NeuroBayesExpert_FOUND)
  include_directories(${NEUROBAYESEXPERT_INCLUDE_DIRS})
  add_definitions(-DHAVE_NEUROBAYES)
endif()

gaudi_add_module(JetTagging
                 src/*.cpp
                 LINK_LIBRARIES LoKiAlgo LHCbMathLib DaVinciInterfacesLib ROOT)

if(NeuroBayesExpert_FOUND)
  target_link_libraries(JetTagging ${NEUROBAYESEXPERT_LIBRARIES})
endif()

gaudi_add_dictionary(JetTagging
                     dict/JetTaggingInterface.h
                     dict/JetTaggingInterface.xml
                     LINK_LIBRARIES GaudiKernel TrackEvent RecEvent PhysEvent
                     OPTIONS " -U__MINGW32__ ")

gaudi_install_python_modules()
