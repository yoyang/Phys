###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Simple functions to steer raw event locations
"""


def setTriggerRawEventLocation(location=None):
    raise ImportError(
        "The contents of this file have been superseded by the DAQSys configurable, please contact Rob Lambert. See savannah task #19106."
    )
