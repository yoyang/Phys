/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ResolvedPi0Maker.h,v 1.5 2009-04-23 10:39:31 pkoppenb Exp $
#ifndef RESOLVEDPI0MAKER_H
#define RESOLVEDPI0MAKER_H 1

// Include files
#include "Kernel/ICaloParticleMaker.h"
#include "Pi0MakerBase.h"

namespace LHCb {
  class ProtoParticle;
  class CaloHypo;
} // namespace LHCb

/** @class ResolvedPi0Maker ResolvedPi0Maker.h
 *
 *  The specialized producer of Resolved Pi0
 *  Can also do eta, and anything that decays to two photons.
 *
 *  @author Olivier Deschamps odescham@in2p3.fr
 *  @date   2006-08-25
 */

class ResolvedPi0Maker : public Pi0MakerBase {

public:
  ResolvedPi0Maker( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~ResolvedPi0Maker();

  StatusCode initialize() override;

private:
  // Make the particles
  StatusCode makeParticles( LHCb::Particle::Vector& particles ) override;

  /** accessor to the specialized tool for estimation of
   *  photon parameters
   */
  bool       selPi0( const LHCb::CaloParticle& g1, const LHCb::CaloParticle& g2 );
  StatusCode makePi0( const LHCb::CaloParticle& g1, const LHCb::CaloParticle& g2, LHCb::Particle* pi0 );

private:
  std::string         m_photonMakerType;
  ICaloParticleMaker* m_photonMaker;

  bool   m_singlePhotonUse;
  bool   m_independantPhotons;
  double m_maxbal;
  double m_minbal;
};
#endif // RESOLVEDPI0MAKER_H
