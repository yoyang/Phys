/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FunctionalParticleMaker.h"

DECLARE_COMPONENT( FunctionalParticleMaker )

FunctionalParticleMaker::FunctionalParticleMaker( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"InputProtoParticles", LHCb::ProtoParticleLocation::Charged}},
                   {KeyValue{"Particles", ""}} ) {}

StatusCode FunctionalParticleMaker::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) { return sc; }

  const std::map<std::string, std::string> id_to_descriptor = {
      {"pion", "pi+"}, {"kaon", "K+"}, {"muon", "mu+"}, {"electron", "e+"}, {"proton", "p+"}};
  const auto result = id_to_descriptor.find( m_particleid.value() );
  if ( result == id_to_descriptor.end() ) { return Error( "Unknown ParticleID value: " + m_particleid.value() ); }
  auto descriptor = ( *result ).second;

  m_particlePropertySvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
  m_particle_prop       = m_particlePropertySvc->find( descriptor );
  if ( !m_particle_prop ) { return Error( "Could not find ParticleProperty for " + m_particleid.value() ); }
  m_antiparticle_prop = m_particle_prop->antiParticle();

  return sc;
}

LHCb::Particles FunctionalParticleMaker::operator()( LHCb::ProtoParticles const& protos ) const {
  LHCb::Particles particles;
  int             nparticles     = 0;
  int             nantiparticles = 0;

  for ( const auto* proto : protos ) {
    const auto* track = proto->track();
    // Sanity checks
    if ( track == nullptr ) {
      Warning( "Charged ProtoParticle with no Track found. Ignoring." ).ignore();
      continue;
    }
    if ( track->states().empty() ) {
      Warning( "Track has empty states. This is likely to be bug" ).ignore();
      continue;
    }

    const auto selected_track = m_track_selector->accept( *track );
    m_npassed_track_filter += selected_track;
    if ( !selected_track ) { continue; }

    const auto selected_proto = m_proto_selector->isSatisfied( proto );
    m_npassed_proto_filter += selected_proto;
    if ( !selected_proto ) { continue; }

    // When filling PID, run additional checks and selections
    if ( m_check_pid.value() ) { check_pid( proto ); }

    // Get the (anti)particle property corresponding to the charge we have
    const LHCb::ParticleProperty* prop = nullptr;
    if ( proto->charge() == m_particle_prop->charge() ) {
      prop = m_particle_prop;
    } else if ( proto->charge() == m_antiparticle_prop->charge() ) {
      prop = m_antiparticle_prop;
    }

    // Fill the particle information
    auto* particle = new LHCb::Particle();
    bool  filled   = fill_particle( proto, prop, particle );
    if ( !filled ) {
      Warning( "Failed to fill Particle, rejecting" ).ignore();
      delete particle;
      particle = nullptr;
      continue;
    }

    // Record what we successfully fill
    if ( prop == m_particle_prop ) {
      ++nparticles;
    } else if ( prop == m_antiparticle_prop ) {
      ++nantiparticles;
    }

    particles.add( particle );
  }

  m_nparticles += nparticles;
  m_nantiparticles += nantiparticles;
  return particles;
}

const LHCb::State* FunctionalParticleMaker::usedState( const LHCb::Track* track ) const {
  const LHCb::State* state = nullptr;
  // Default: closest to the beam for track types with a Velo part.
  if ( !( track->checkType( LHCb::Track::Types::Downstream ) || track->checkType( LHCb::Track::Types::Ttrack ) ) ) {
    state = track->stateAt( LHCb::State::Location::ClosestToBeam );
  }
  // If not available: first measurement. Default for Downstream and T tracks
  if ( !state ) { state = track->stateAt( LHCb::State::Location::FirstMeasurement ); }
  // Backup
  if ( !state ) {
    Warning( "Found no state at ClosestToBeam or at FirstMeasurement for track. Using first state instead",
             StatusCode{10}, 1 )
        .ignore();
    state = &track->firstState();
  }
  return state;
}

void FunctionalParticleMaker::check_pid( const LHCb::ProtoParticle* proto ) const {
  // RICH links
  if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::RichPIDStatus ) ) {
    const LHCb::RichPID* rpid = proto->richPID();
    if ( !rpid ) { Error( "ProtoParticle has RICH information but NULL RichPID SmartRef!" ).ignore(); }
  }
  // Muon links
  if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::MuonPIDStatus ) ) {
    const LHCb::MuonPID* mpid = proto->muonPID();
    if ( !mpid ) { Error( "ProtoParticle has MUON information but NULL MuonPID SmartRef!" ).ignore(); }
  }
}

bool FunctionalParticleMaker::fill_particle( const LHCb::ProtoParticle* proto, const LHCb::ParticleProperty* property,
                                             LHCb::Particle* particle ) const {
  particle->setMeasuredMass( property->mass() );
  particle->setMeasuredMassErr( 0 );

  particle->setParticleID( property->particleID() );
  particle->setProto( proto );

  // Take the default confidence level
  particle->setConfLevel( m_CL );

  // Find the appropriate state and use it to define this object's kinematics
  const LHCb::State* state = usedState( proto->track() );
  auto               sc    = m_particle_from_state_tool->state2Particle( *state, *particle );
  if ( sc.isFailure() ) {
    Warning( "Failed to fill Particle from State", sc ).ignore();
    return false;
  }

  return true;
}
