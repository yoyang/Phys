/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ADDEXTRAINFO_H
#define ADDEXTRAINFO_H 1

#include <algorithm>
#include <sstream>
#include <string>

// Include files
#include "Kernel/IExtraInfoTool.h"

#include "Kernel/DaVinciAlgorithm.h"

/** @class AddExtraInfo AddExtraInfo.h
 *
 *  Algorithm to add Particle isolation variables
 *  calculated by ConeVariables tool to particle extra info
 *
 *  @author Anton Poluektov
 *  @date   19/02/2012
 */

class AddExtraInfo : public DaVinciAlgorithm {

public:
  /// Standard constructor
  AddExtraInfo( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~AddExtraInfo();          ///< Destructor
  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  std::vector<std::string>   m_toolNames;
  std::list<IExtraInfoTool*> m_tools;
  int                        m_maxLevel;
  std::vector<std::string>   m_daughterLocations;

  void fill( const LHCb::Particle* top, LHCb::Particle* c, int level );
};

//=======================================================================//
#endif // AddExtraInfo_H
