/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IREPORTCHECK_H
#define IREPORTCHECK_H 1

#include "GaudiKernel/IAlgTool.h"
#include <string>

static const InterfaceID IID_IReportCheck( "IReportCheck", 1, 0 );
class IReportCheck : virtual public IAlgTool {

public:
  /// Retrieve interface ID
  static const InterfaceID& interfaceID() { return IID_IReportCheck; } // Actual operator function
  virtual int               VersionTopLevel( std::string, std::string ) = 0;
  virtual int               checkBankVersion()                          = 0;
  virtual unsigned int      getTCK()                                    = 0;
};

#endif
