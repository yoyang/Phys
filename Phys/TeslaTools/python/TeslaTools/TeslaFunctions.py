###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def TeslaDVRedoAlgs(lines):
    from Configurables import GaudiSequencer
    seq = GaudiSequencer("TeslaDVRedoAlgs")
    from Configurables import EventNodeKiller
    enk = EventNodeKiller('KillTeslaInput')
    enk.Nodes = [
        "/Event/Turbo/pPhys/Particles", "/Event/Turbo/pPhys/Vertices",
        "/Event/Turbo/pPhys/RecVertices", "/Event/Turbo/pPhys/Relations",
        "/Event/Turbo/pPhys/Relations", "/Event/Turbo/pPhys/PP2MCPRelations",
        "/Event/Turbo/pRec/Track/Custom", "/Event/Turbo/pRec/Muon/CustomPIDs",
        "/Event/Turbo/pRec/Rich/CustomPIDs",
        "/Event/Turbo/pRec/neutrals/Clusters",
        "/Event/Turbo/pRec/neutrals/Hypos", "/Event/Turbo/pRec/ProtoP/Custom"
    ]
    from Configurables import TeslaReportAlgo
    repAlg = TeslaReportAlgo("RedoTesla")
    repAlg.TriggerLines = lines
    seq.Members += [enk, repAlg]
    return seq
