/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ADDRELATEDINFO_H
#define ADDRELATEDINFO_H 1

// Kernel
#include "Kernel/DaVinciAlgorithm.h"
#include "Kernel/IRelatedInfoTool.h"
#include "LoKi/ChildSelector.h"
#include "Relations/Relations.h"

// boost
#include "boost/algorithm/string.hpp"

// event model
#include "Event/RelatedInfoMap.h"

/** @class AddRelatedInfo AddRelatedInfo.h
 *
 *  Algorithm to add Particle isolation variables
 *  calculated by ConeVariables tool to particle extra info
 *
 *  @author Anton Poluektov
 *  @date   15/01/2014
 */

typedef LHCb::Relation1D<LHCb::Particle, LHCb::RelatedInfoMap> ParticleInfoRelation;

class AddRelatedInfo : public DaVinciAlgorithm {

public:
  /// Standard constructor
  AddRelatedInfo( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~AddRelatedInfo(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  void fill( const LHCb::Particle* top, const LHCb::Particle* c, const std::string& top_location );

private:
  std::string       m_toolName;
  IRelatedInfoTool* m_tool;

  typedef std::map<std::string, std::string> InfoMap;
  InfoMap                                    m_daughterInfo;

  std::string m_topInfo;

  typedef std::map<std::string, std::unique_ptr<LoKi::Child::Selector>> SelectorMap;
  SelectorMap                                                           m_childSelectors;

  typedef std::map<std::string, ParticleInfoRelation> RelationLocationMap;
  mutable RelationLocationMap                         m_relMap;

  bool m_ignoreUnmatchedDescriptors;
};

//=======================================================================//
#endif // AddRelatedInfo_H
