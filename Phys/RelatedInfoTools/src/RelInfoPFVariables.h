/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RELINFOPFVARIABLES_H
#define RELINFOPFVARIABLES_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IRelatedInfoTool.h" // Interface

struct IDVAlgorithm;
typedef std::map<std::string, double> MapStringDouble;

/** @class RelinfoPFVariables RelinfoPFVariables.h
 *
 *
 *  @author Xabier Cid Vidal
 *  @date   2014-06-30
 */
class RelInfoPFVariables : public GaudiTool, virtual public IRelatedInfoTool {

public:
  /// Standard constructor
  RelInfoPFVariables( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  /// Loop over PF particles and fill the variables
  StatusCode calculateRelatedInfo( const LHCb::Particle*, const LHCb::Particle* ) override;

  LHCb::RelatedInfoMap* getInfo() override;

  virtual ~RelInfoPFVariables(); ///< Destructor

private:
  std::string              m_PFLocation; // location of the PFParticles
  std::vector<std::string> m_variables;
  std::vector<short int>   m_keys;

  MapStringDouble m_properties; // includes all the output properties values

  LHCb::RelatedInfoMap m_map;

  double invariantMass( std::vector<Gaudi::LorentzVector> mypars );
  void   emptyProperties();
  void   fillMap(); // fill the output map
};

#endif // RELINFOPFVARIABLES_H
