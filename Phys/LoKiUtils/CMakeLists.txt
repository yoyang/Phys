###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: LoKiUtils
################################################################################
gaudi_subdir(LoKiUtils)

gaudi_depends_on_subdirs(Phys/DaVinciInterfaces
                         Phys/LoKiCore
                         Tr/TrackInterfaces)

find_package(ROOT)
find_package(Boost)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_library(LoKiUtils
                  src/*.cpp
                  PUBLIC_HEADERS LoKi
                  INCLUDE_DIRS Tr/TrackInterfaces
                  LINK_LIBRARIES DaVinciInterfacesLib LoKiCoreLib)

gaudi_add_dictionary(LoKiUtils
                     dict/LoKiUtilsDict.h
                     dict/LoKiUtils.xml
                     INCLUDE_DIRS Tr/TrackInterfaces
                     LINK_LIBRARIES DaVinciInterfacesLib LoKiCoreLib LoKiUtils
                     OPTIONS " -U__MINGW32__ ")

gaudi_install_python_modules()

