###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: DaVinciMCKernel
################################################################################
gaudi_subdir(DaVinciMCKernel)

gaudi_depends_on_subdirs(Associators/MCAssociators
                         Event/GenEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         Event/PhysEvent
                         Event/RecEvent
                         Phys/DaVinciInterfaces
                         Phys/LoKiCore)

find_package(ROOT)
find_package(Boost)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_library(DaVinciMCKernelLib
                  src/Lib/*.cpp
                  PUBLIC_HEADERS Kernel
                  INCLUDE_DIRS Associators/MCAssociators
                  LINK_LIBRARIES GenEvent LinkerEvent MCEvent PhysEvent RecEvent DaVinciInterfacesLib LoKiCoreLib)

gaudi_add_dictionary(DaVinciMCKernel
                     dict/DaVinciMCKernelDict.h
                     dict/DaVinciMCKernelDict.xml
                     INCLUDE_DIRS Associators/MCAssociators
                     LINK_LIBRARIES GenEvent LinkerEvent MCEvent PhysEvent RecEvent DaVinciInterfacesLib LoKiCoreLib DaVinciMCKernelLib
                     OPTIONS "-U__MINGW32__")

