/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IInterface.h"
// ============================================================================
// Kernel?
// ============================================================================
#include "Kernel/IHepMC2MC.h"
// ============================================================================
/** @file
 *
 *  Implementation file for class IHepMC2MC
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-01-23
 */
// ============================================================================
namespace {
  // ==========================================================================
  /** @var IID_IHepMC2MC
   *  unique static identifier of IHepMC2MC interface
   *  @see IHepMC2MC
   *  @author Vanya BELYAEV Ivan.Belyaev@lapp.in2p3.fr
   *  @date 2005-05-12
   */
  // ==========================================================================
  const InterfaceID IID_IHepMC2MC( "IHepMC2MC", 1, 0 );
  // ==========================================================================
} // namespace
// ============================================================================
// Return the unique interface identifier
// ============================================================================
const InterfaceID& IHepMC2MC::interfaceID() { return IID_IHepMC2MC; }
// ============================================================================
// destructor
// ============================================================================
IHepMC2MC::~IHepMC2MC() {}
// ============================================================================
// The END
// ============================================================================
