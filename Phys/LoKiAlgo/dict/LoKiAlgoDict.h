/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_LOKIALGODICT_H
#  define LOKI_LOKIALGODICT_H 1
// ============================================================================
// redefined anyway in features.h by _GNU_SOURCE
#  undef _XOPEN_SOURCE
#  undef _POSIX_C_SOURCE
// Python must always be the first.
#  include "Python.h"
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/AlgoDecorator.h"
#  include "LoKi/ExtCalls.h"
#  include "LoKi/LoKiAlgo.h"
#  include "LoKi/LoopChild.h"
#  include "LoKi/LoopDecorator.h"
#  include "LoKi/TupleDicts.h"
// ============================================================================
namespace LoKiAlgoDict {
  // ===========================================================================
  struct __Instantiations {
    LoKi::Dicts::ExtFunCalls<LHCb::Particle>   m_c1;
    LoKi::Dicts::ExtFunCalls<LHCb::VertexBase> m_c2;
    LoKi::Dicts::ExtCutCalls<LHCb::Particle>   m_c3;
    LoKi::Dicts::ExtCutCalls<LHCb::VertexBase> m_c4;
    //
    LoKi::Dicts::Alg<LoKi::Algo> m_a1;
    // fictive constructor
    __Instantiations();
    ~__Instantiations();
  };
  // ==========================================================================
} // namespace LoKiAlgoDict
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_LOKIALGODICT_H
// ============================================================================
