/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: L0TriggerTisTos.h,v 1.1 2010-06-23 22:39:24 tskwarni Exp $
#ifndef L0TRIGGERTISTOS_H
#define L0TRIGGERTISTOS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/ITriggerTisTos.h" // Interface

#include "TriggerTisTos.h"

/** @class L0TriggerTisTos L0TriggerTisTos.h
 *
 *  @author Tomasz Skwarnicki
 *  @date   2010-06-23
 *
 *  Same tool as TriggerTisTos; just different input locations
 */
class L0TriggerTisTos : public TriggerTisTos, virtual public ITriggerTisTos {
public:
  /// Standard constructor
  L0TriggerTisTos( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~L0TriggerTisTos(); ///< Destructor

  StatusCode initialize() override;

private:
};
#endif // L0TRIGGERTISTOS_H
