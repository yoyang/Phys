/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PARTICLETRAJPROVIDER_H
#define PARTICLETRAJPROVIDER_H 1

// Include files
// -------------
#include "GaudiKernel/IAlgTool.h"

// From TrackEvent
#include "TrajParticle.h"
struct ITrackStateProvider;

namespace ParticleTrajProvider {
  // some global functions to create TrajParticles
  std::unique_ptr<LHCb::TrajParticle> trajectory( const ITrackStateProvider& stateprovider,
                                                  const LHCb::Particle&      particle );
  std::unique_ptr<LHCb::TrajParticle> trajectory( const ITrackStateProvider& stateprovider,
                                                  const LHCb::Track&         particle );
} // namespace ParticleTrajProvider

#endif
