/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Gaudi
#include "Event/Particle.h"
#include "Event/Vertex.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/IGenericVertexFinder.h"
#include "ParticleLocation.h"

/** @class GenericVertexFinderAlg GenericVertexFinderAlg.cpp
 *
 * GaudiAlgorithm that takes looks for vertices in a list of input
 * particles. Use case: secondary vertex finding in Std::Pions.
 *
 * The algorithm searches for vertices in the list p_1 ... p_N. For
 * every found vertex a new LHCb::Particle P is created. Its daughters
 * are the particles {p_i ... p_j} that formed the vertex. The
 * particles P stored in the location specified by Output.
 *
 *  @author Wouter HULSBERGEN
 **/
class GenericVertexFinderAlg : public GaudiAlgorithm {

public:
  // Constructors and destructor
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode execute() override;
  StatusCode initialize() override;

private:
  ToolHandle<IGenericVertexFinder>          m_vertexfinder{this, "VertexFinder", "GenericVertexFinder"};
  Gaudi::Property<Kernel::ParticleLocation> m_inputLocation{this, "Input", "", "Location of input particles"};
  Gaudi::Property<Kernel::ParticleLocation> m_outputLocation{this, "Output", "", "Location of output particles"};
};

DECLARE_COMPONENT( GenericVertexFinderAlg )

StatusCode GenericVertexFinderAlg::initialize() {
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "InputParticleLocation: '" << m_inputLocation.value().particles() << "'" << std::endl
            << "OutputParticleLocation: '" << m_outputLocation.value().particles() << "'" << std::endl
            << "OutputVertexLocation: '" << m_outputLocation.value().vertices() << "'" << endmsg;
  }
  return GaudiAlgorithm::initialize();
}

StatusCode GenericVertexFinderAlg::execute() {

  // get the particles
  LHCb::Particle::Range input = get<LHCb::Particle::Range>( m_inputLocation.value().particles() );
  // turn it into a vector
  LHCb::Particle::ConstVector particles( input.begin(), input.end() );
  // call the verexing
  LHCb::Particles* outputparticles = new LHCb::Particles();
  LHCb::Vertices*  outputvertices  = new LHCb::Vertices();
  put( outputparticles, m_outputLocation.value().particles() );
  put( outputvertices, m_outputLocation.value().vertices() );

  for ( auto& p : m_vertexfinder->findVertices( particles ) ) {
    outputparticles->add( p.particle.release() );
    outputvertices->add( p.vertex.release() );
  }
  return StatusCode::SUCCESS;
}
