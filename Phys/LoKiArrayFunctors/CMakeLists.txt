###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: LoKiArrayFunctors
################################################################################
gaudi_subdir(LoKiArrayFunctors)

gaudi_depends_on_subdirs(Phys/LoKiPhys
                         Phys/LoKiProtoParticles
			 Phys/DaVinciKernel)

find_package(Boost)
find_package(CLHEP)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(LoKiArrayFunctorsLib
                  src/*.cpp
                  PUBLIC_HEADERS LoKi
                  LINK_LIBRARIES LoKiPhysLib LoKiProtoParticles)

gaudi_add_module(LoKiArrayFunctors
                 src/Components/*.cpp
                 LINK_LIBRARIES LoKiPhysLib LoKiProtoParticles LoKiArrayFunctorsLib DaVinciKernelLib)

gaudi_add_dictionary(LoKiArrayFunctors
                     dict/LoKiArrayFunctorsDict.h
                     dict/LoKiArrayFunctors.xml
                     LINK_LIBRARIES LoKiPhysLib LoKiProtoParticles LoKiArrayFunctorsLib
                     OPTIONS " -U__MINGW32__ ")

gaudi_install_python_modules()


gaudi_add_test(QMTest QMTEST)
