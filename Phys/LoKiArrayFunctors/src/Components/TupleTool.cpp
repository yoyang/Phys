/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/VectorMap.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiAlg/Tuples.h"
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/IParticleTupleTool.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/IHybridFactory.h"
#include "LoKi/PhysTypes.h"
// ============================================================================
// Boost
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
// Local
// ============================================================================
#include "TupleTool.h"
// ============================================================================
// initialization of the tool
// ============================================================================
LoKi::Hybrid::TupleTool::TupleTool( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent )
    , m_factory( "LoKi::Hybrid::Tool/HybridFactory:PUBLIC" )
    , m_vars()
    , m_items()
    , m_preambulo() {
  declareInterface<IParticleTupleTool>( this );
  ///
  if ( 0 == name.find( "Hlt1" ) ) {
    m_factory = "LoKi::Hybrid::Tool/Hlt1HybridFactory:PUBLIC";
  } else if ( 0 == name.find( "Hlt2" ) ) {
    m_factory = "LoKi::Hybrid::Tool/Hlt2HybridFactory:PUBLIC";
  }
  //
  declareProperty( "Factory", m_factory, "Type/Name for C++/Python Hybrid Factory" )
      ->declareUpdateHandler( &LoKi::Hybrid::TupleTool::propHandler, this );

  //
  declareProperty( "Variables", m_vars, "The {'name':'functor'}-map of columns for N-tuple " )
      ->declareUpdateHandler( &LoKi::Hybrid::TupleTool::propHandler, this );

  // the preambulo
  declareProperty( "Preambulo", m_preambulo, "The preambulo to be used for Bender/Python script" )
      ->declareUpdateHandler( &LoKi::Hybrid::TupleTool::propHandler, this );
  //
}
// ======================================================================
// initialization of the tool
// ======================================================================
StatusCode LoKi::Hybrid::TupleTool::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return sc; } // RETURN
  svc<IService>( "LoKiSvc", true );
  //
  return initVariables();
}
// ======================================================================
// finalization of the tool
// ======================================================================
StatusCode LoKi::Hybrid::TupleTool::finalize() {
  // reset all functors:
  m_items.clear();
  // finalize the base
  return GaudiTool::finalize();
}
// ======================================================================
// the update handler
// ======================================================================
void LoKi::Hybrid::TupleTool::propHandler( Property& /* p */ ) {
  //
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  //
  Warning( "Reintialization of Variables/Factory/Preambulo/..." ).ignore();
  //
  StatusCode sc = initVariables();
  Assert( sc.isSuccess(), "Unable to set 'Variables'", sc );
}
// ======================================================================
// initializr variables
// ======================================================================
StatusCode LoKi::Hybrid::TupleTool::initVariables() {
  // get the factory
  IHybridFactory* the_factory = tool<IHybridFactory>( factory(), this );
  //
  m_items.clear();
  m_items.reserve( m_vars.size() );
  for ( Map::const_iterator ivar = m_vars.begin(); m_vars.end() != ivar; ++ivar ) {
    Item       item;
    StatusCode sc = the_factory->get( ivar->second, item.m_fun, preambulo() );
    if ( sc.isFailure() ) { return Error( "Unable to decode " + ivar->first + " : " + ivar->second, sc ); }
    //
    item.m_name = ivar->first;
    m_items.emplace_back( ivar->first, item );
    //
    debug() << "The decoded variable name is '" << m_items.back().first << "'\t, the functor : '"
            << m_items.back().second.m_fun << "'" << endmsg;
  }
  //
  if ( m_items.empty() ) { Warning( "No variables/items are defined" ).ignore(); }
  //
  release( the_factory ).ignore(); // we do not need the factory anymore
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
/*  Fill the tuple.
 *  @see IParticleTupleTool
 *  @param top      the top particle of the decay.
 *  @param particle the particle about which some info are filled.
 *  @param head     prefix for the tuple column name.
 *  @param tuple    the tuple to fill
 *  @return status code
 */
// ============================================================================
StatusCode LoKi::Hybrid::TupleTool::fill( const LHCb::Particle* top, const LHCb::Particle* particle,
                                          const std::string& head, Tuples::Tuple& tuple ) {
  if ( !top ) { Warning( "LHCb::Particle* 'Top' points to NULL" ).ignore(); }
  if ( !particle ) { Warning( "LHCb::Particle*       points to NULL" ).ignore(); }
  //
  if ( !tuple.valid() ) { return Error( "Invalid tuple " ); }
  //
  std::string head_ = head + "_";
  //
  for ( const auto& item : items() ) {
    // fill N-tuple
    tuple->column( head_ + item.first, item.second( particle ) ).ignore();
  }
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
/// Declaration of the Tool Factory
DECLARE_COMPONENT( LoKi::Hybrid::TupleTool )
// ============================================================================
// The END
// ============================================================================
