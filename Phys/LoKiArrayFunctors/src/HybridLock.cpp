/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/HybridLock.h"
#include "LoKi/HybridEngineActor.h"
#include "LoKi/Report.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::HybridLock
 *  @date 2007-06-10
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
// contructor
// ============================================================================
LoKi::Hybrid::Lock::Lock( const IHybridTool* factory, const LoKi::Context& context ) : m_factory( factory ) {
  LoKi::Hybrid::EngineActor& actor = LoKi::Hybrid::EngineActor::instance();
  // connect the tool to the actor
  StatusCode sc = actor.connect( m_factory, context );
  if ( sc.isFailure() ) { LoKi::Report::Error( "LoKi::Hybrid::Lock: error from connectTool", sc ).ignore(); }
}
// ============================================================================
// destructor
// ============================================================================
LoKi::Hybrid::Lock::~Lock() {
  LoKi::Hybrid::EngineActor& actor = LoKi::Hybrid::EngineActor::instance();
  // connect the tool to the actor
  StatusCode sc = actor.disconnect( m_factory );
  if ( sc.isFailure() ) { LoKi::Report::Error( "LoKi::Hybrid::Lock: error from releaseTool", sc ).ignore(); }
}
// ============================================================================
// The END
// ============================================================================
