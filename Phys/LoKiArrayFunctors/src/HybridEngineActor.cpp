/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <fstream>
#include <sstream>
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/HybridEngineActor.h"
#include "LoKi/Report.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::HybridEngineActor
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @date 2004-06-29
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 */
// ============================================================================
// helper method to descrease number of lines
// ============================================================================
template <class TYPE>
inline StatusCode LoKi::Hybrid::EngineActor::_add( const std::string& name, const TYPE& cut ) const {
  // ==========================================================================
  // check the tool
  if ( m_stack.empty() ) { return LoKi::Report::Error( "LoKi:Hybrid::EngineActor::addCut/Fun(): stack is empty!" ); }
  //
  const Entry& entry = m_stack.top();
  if ( !entry.first ) { return LoKi::Report::Error( "LoKi:Hybrid::EngineActor::addCut/Fun(): invalid tool!" ); }
  // ==========================================================================
  // one more check
  if ( name != entry.first->name() ) {
    return LoKi::Report::Error( "LoKi:Hybrid::EngineActor::addCut/Fun() : mismatch in LoKi::IHybridTool name!" );
  }
  // ==========================================================================
  // set the cut for the tool
  entry.first->set( cut );
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
// accessor to the static instance
// ============================================================================
LoKi::Hybrid::EngineActor& LoKi::Hybrid::EngineActor::instance() {
  static LoKi::Hybrid::EngineActor s_holder;
  return s_holder;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Hybrid::EngineActor::EngineActor() : m_stack() {}
// ============================================================================
// destructor
// ============================================================================
LoKi::Hybrid::EngineActor::~EngineActor() {}
// ============================================================================
// disconnect the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::disconnect( const LoKi::IHybridTool* tool ) {
  if ( m_stack.empty() ) { return LoKi::Report::Error( "LoKi:Hybrid::EngineActor::disconnect: empty stack!" ); }
  //
  const Entry& entry = m_stack.top();
  if ( entry.first != tool ) {
    return LoKi::Report::Error( "LoKi:Hybrid::EngineActor::disconnect: mismatch in tools " );
  }
  // POP it!
  m_stack.pop();
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
// connect the hybrid tool for code translation
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::connect( const LoKi::IHybridTool* factory, const LoKi::Context& context )

{
  //
  if ( !factory ) { return LoKi::Report::Error( "LoKi::Hybrid::EngineActor::connect: Invalid factory" ); }
  m_stack.emplace( factory, context );
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
/* get the current context
 *  contex is valid only inbetween <code>connect/disconnect</code>
 *  @return the current active context
 */
// ============================================================================
const LoKi::Context* LoKi::Hybrid::EngineActor::context() const {
  if ( m_stack.empty() ) {
    LoKi::Report::Error( "LoKi::Hybrid::EngineActor::context: empty stack!" ).ignore();
    return nullptr;
  }
  const Entry& last = m_stack.top();
  return &last.second;
}
// ============================================================================
// predicates
// ============================================================================
// propagate the cut to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::Cuts& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the cut to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::VCuts& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the cut to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::ACuts& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the cut to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::PPCuts& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// functions
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::Func& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::VFunc& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::AFunc& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::PPFunc& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// maps
// ============================================================================
// propagate the map  to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::Maps& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the map to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::VMaps& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the map to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::PPMaps& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// pipes
// ============================================================================
// propagate the pipe to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::Pipes& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the pipe to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::VPipes& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the pipe to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::PPPipes& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// fun-vals
// ============================================================================
// propagate the fun-val  to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::FunVals& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the fun-val to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::VFunVals& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the fun-val to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::PPFunVals& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// cut-vals
// ============================================================================
// propagate the fun-val  to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::CutVals& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the fun-val to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::VCutVals& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the fun-val to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::PPCutVals& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// sources
// ============================================================================
// propagate the source to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::Sources& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the source to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::VSources& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the source to the tool
// ============================================================================
StatusCode LoKi::Hybrid::EngineActor::process( const std::string& name, const LoKi::Types::PPSources& cut ) const {
  return _add( name, cut );
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
