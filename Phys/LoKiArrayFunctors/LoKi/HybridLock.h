/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_HYBRIDLOCK_H
#  define LOKI_HYBRIDLOCK_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/Context.h"
#  include "LoKi/IHybridTool.h"
#  include "LoKi/Interface.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Hybrid {
    // ========================================================================
    /** @class Lock LoKi/HybridLock.h
     *  Helper class (sentry) to conhent IHynbdirTool to HybridEngine
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2007-06-09
     */
    class Lock {
      // ======================================================================
    public:
      // ======================================================================
      /// constructor : Lock
      Lock( const IHybridTool* factory, const LoKi::Context& context );
      /// destructor  : UnLock
      virtual ~Lock(); // destrcutor : UnLock
      // ======================================================================
    private:
      // ======================================================================
      /// no default constructor
      Lock(); // no default constructor
      /// no copy constructor
      Lock( const Lock& ); //    no copy constructor
      /// no assignement opeartor
      Lock& operator=( const Lock& ); // no assignement operator
      // ======================================================================
    private:
      // ======================================================================
      /// the tool itself
      LoKi::Interface<LoKi::IHybridTool> m_factory; // the tool itself
      // ======================================================================
    };
    // ========================================================================
  } //                                                  end of namespace Hybrid
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_HYBRIDLOCK_H
// ============================================================================
