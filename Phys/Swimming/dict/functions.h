/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef SWIMMING_FUNCTIONS_H
#define SWIMMING_FUNCTIONS_H 1

#include <string>

#include <boost/integer/integer_mask.hpp>
#include <boost/integer_traits.hpp>

#include <Swimming/Interfaces.h>

namespace Random {
  boost::uint32_t mix( boost::uint32_t state );
  boost::uint32_t mix32( boost::uint32_t state, boost::uint32_t extra );
  boost::uint32_t mix64( boost::uint32_t state, boost::uint64_t extra );
  boost::uint32_t mixString( boost::uint32_t state, const std::string& extra );
} // namespace Random

#endif // SWIMMING_FUNCTIONS_H
