/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef SWIMMING_HACKS_H
#define SWIMMING_HACKS_H 1

#include <map>
#include <string>
#include <vector>

namespace LHCb {
  class TurningPoint;
}

namespace SwimmingHacks {
  LHCb::TurningPoint* createTurningPoint( const double raw, const double tau, const double ip, const double dec,
                                          const std::map<std::string, bool>&                   decisions,
                                          const std::map<std::string, std::map<size_t, bool>>& info,
                                          const std::vector<std::pair<std::string, double>>&   extra );
}

#endif // SWIMMING_HACKS_H
