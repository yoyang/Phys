###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#General options for swimming the trigger
from Configurables import Moore, HltConfigSvc
#Global configuration
Moore().UseTCK = True
Moore().L0 = True
Moore().ReplaceL0BanksWithEmulated = False
Moore().UseDBSnapshot = False
Moore().EnableRunChangeHandler = False
Moore().CheckOdin = False
Moore().Verbose = False
Moore().EnableDataOnDemand = True
HltConfigSvc().optionsfile = '/tmp/dump.opts'
