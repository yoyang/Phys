/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PVREFITTER_H
#define PVREFITTER_H 1

// Include files
// from Gaudi
#include <GaudiAlg/GaudiTool.h>

// DaVinci
#include <Kernel/IPVReFitter.h>

// Event
#include <Event/RecVertex.h>

class IIncidentSvc;

namespace Swimming {
  namespace Interfaces {
    class IService;
  }

  /** @class PVReFitter PVReFitter.h
   *
   *  A tool to forward refitting a PV to another tool and moving the vertex afterwards.
   *
   *  @author Roel Aaij
   *  @date   2011-11-24
   */
  class PVReFitter : public extends1<GaudiTool, IPVReFitter> {
  public:
    /// Standard constructor
    PVReFitter( const std::string& type, const std::string& name, const IInterface* parent );

    StatusCode initialize() override;

    /// refit PV
    StatusCode reFit( LHCb::VertexBase* PV ) const override;

    /// remove track used for a (B) LHCb::Particle and refit PV
    StatusCode remove( const LHCb::Particle* part, LHCb::VertexBase* PV ) const override;

    virtual ~PVReFitter(); ///< Destructor

  private:
    std::string m_refitterName;
    std::string m_serviceName;

    const IPVReFitter*    m_refitter;
    Interfaces::IService* m_service;
  };

} // namespace Swimming
#endif // PVREFITTER_H
