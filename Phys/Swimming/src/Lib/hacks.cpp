/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// stdlib
#include <map>
#include <string>
#include <vector>

// LHCb
#include <Event/TurningPoint.h>
namespace SwimmingHacks {

  LHCb::TurningPoint* createTurningPoint( const double raw, const double tau, const double ip, const double dec,
                                          const std::map<std::string, bool>&                   decisions,
                                          const std::map<std::string, std::map<size_t, bool>>& info,
                                          const std::vector<std::pair<std::string, double>>&   extra ) {
    std::map<std::string, double> extra_fudged;
    for ( std::vector<std::pair<std::string, double>>::const_iterator i = extra.begin(); i != extra.end(); i++ )
      extra_fudged[i->first] = i->second;
    return new LHCb::TurningPoint( raw, tau, ip, dec, decisions, info, extra_fudged );
  }
} // namespace SwimmingHacks
