/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef SWIMMINGINTERFACES_H
#define SWIMMINGINTERFACES_H 1

// Include files
#include <GaudiKernel/IInterface.h>
#include <GaudiKernel/IService.h>
#include <GaudiKernel/Vector3DTypes.h>

// Event
#include "Event/Track.h"
#include <Event/RecVertex.h>

namespace LHCb {
  class Particle;
}
struct IPVReFitter;

namespace Swimming {

  namespace Interfaces {

    class GAUDI_API IService : virtual public extend_interfaces1<::IService> {
    public:
      virtual ~IService(){};

      virtual Gaudi::XYZVector offset() const = 0;

      virtual void setOffset( const Gaudi::XYZVector& offset ) = 0;

      virtual LHCb::RecVertex::ConstVector getOfflinePVs( const LHCb::Particle* particle,
                                                          const IPVReFitter*    refitter = 0 ) = 0;

      virtual void                                   setSignalTracks( const LHCb::Particle* candidate = 0 ) = 0;
      virtual const std::vector<const LHCb::Track*>& getSignalTracks() const                                = 0;

      DeclareInterfaceID( Interfaces::IService, 1, 0 );
    };
  } // namespace Interfaces
} // namespace Swimming
#endif // SWIMMINGINTERFACES_H
