###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This file provides configurations for the Phys/FlavourTagging tools.
It is available from the FlavourTagging python package.
Each instance of the TaggingTool will need to be configured via

```python
from FlavourTagging.Tunings import applyTuning

applyTuning(my_tagging_tool)
```
See `applyTuning` for more details.
"""

import sys
from copy import deepcopy

from Configurables import (
    BTaggingTool, )

import OSCharmTaggerConf
import OSElectronTaggerConf
import OSKaonTaggerConf
import OSMuonTaggerConf
import OSVtxChTaggerConf
import SSJetTaggerConf
import SSKaonTaggerConf
import SSPionTaggerConf
import SSProtonTaggerConf
import DevelopmentTaggerConf


class BaseTuning(object):
    """
    A Tuning base class
    """
    enabled_taggers_map = {}
    call_order = []
    # preselection cut_params are currently defined as BTaggingTool properties
    preselection = {}


class TestTuning(BaseTuning):
    """
    Tuning for nightly tests. See Analysis/Phys/FlavourTaggingChecker and
    DaVinci/DaVinciTests for more details on how and where this tuning is used
    """
    enabled_taggers_map = {
        "OS_Muon": OSMuonTaggerConf.Stripping23,
        "OS_Electron": OSElectronTaggerConf.Stripping23,
        "OS_Kaon": OSKaonTaggerConf.Stripping23,
        "SS_Kaon": SSKaonTaggerConf.Stripping21,
        "SS_Pion": SSPionTaggerConf.Stripping21,
        "VtxCharge": OSVtxChTaggerConf.Stripping23,
        "OS_nnetKaon": OSKaonTaggerConf.Stripping23NNet,
        "SS_nnetKaon": SSKaonTaggerConf.Stripping23NNet,
        "OS_Charm": OSCharmTaggerConf.Stripping23,
        "SS_Proton": SSProtonTaggerConf.ClassicBDT,
        "SS_PionBDT": SSPionTaggerConf.ClassicBDT,
    }


class Run1(BaseTuning):
    """
    Old implementation (taggers optimised on Run 1)
    """
    enabled_taggers_map = {
        "OSCharm": OSCharmTaggerConf.Stripping23,
        "OSElectron": OSElectronTaggerConf.Stripping23,
        "OSKaon": OSKaonTaggerConf.Stripping23,
        "OSMuon": OSMuonTaggerConf.Stripping23,
        "OSVtxCh": OSVtxChTaggerConf.Stripping23,
        "SSJet": SSJetTaggerConf.default,
        "SSKaon": SSKaonTaggerConf.Stripping23NNet,
        "SSPion": SSPionTaggerConf.ClassicBDT,
        "SSProton": SSProtonTaggerConf.ClassicBDT,
    }


class Summer2017Optimisation(BaseTuning):
    """
    New implementation, first iteration.
    Mu, K, e are optimised on Run 2 data.
    """
    enabled_taggers_map = {
        "OSCharm": OSCharmTaggerConf.Stripping23,
        "OSElectron": OSElectronTaggerConf.Stripping23,
        "OSElectronLatest": OSElectronTaggerConf.Summer2017Opt,
        "OSKaon": OSKaonTaggerConf.Stripping23,
        "OSKaonLatest": OSKaonTaggerConf.Summer2017Opt,
        "OSMuon": OSMuonTaggerConf.Stripping23,
        "OSMuonLatest": OSMuonTaggerConf.Summer2017Opt,
        "OSVtxCh": OSVtxChTaggerConf.Stripping23,
        "SSKaon": SSKaonTaggerConf.Stripping23NNet,
        "SSKaonLatest": SSKaonTaggerConf.Stripping23NNetDev,
        "SSPion": SSPionTaggerConf.ClassicBDT,
        "SSProton": SSProtonTaggerConf.ClassicBDT,
    }


class Summer2017Optimisation_Run1(BaseTuning):
    """
    New implementation, final iteration
    e is optimised on Run 1 data.
    (NOMINAL RUN1 TUNINGS)
    """
    enabled_taggers_map = {
        "OSCharm": OSCharmTaggerConf.Stripping23,
        "OSElectron": OSElectronTaggerConf.Stripping23,
        "OSElectronLatest": OSElectronTaggerConf.Summer2017Opt_Run1,
        "OSKaon": OSKaonTaggerConf.Stripping23,
        "OSMuon": OSMuonTaggerConf.Stripping23,
        "OSVtxCh": OSVtxChTaggerConf.Stripping23,
        "SSKaon": SSKaonTaggerConf.Stripping23NNet,
        "SSPion": SSPionTaggerConf.ClassicBDT,
        "SSProton": SSProtonTaggerConf.ClassicBDT,
    }


class Summer2017Optimisation_Run1_noVeloCharge(Summer2017Optimisation_Run1):
    """
    Nominal Run1 tunings (Summer2017Optimisation_Run1)
    without VELO charge cuts (to be used for upgrade studies
    """
    enabled_taggers_map = deepcopy(
        Summer2017Optimisation_Run1.enabled_taggers_map)
    enabled_taggers_map.update({
        'OSElectronLatest':
        OSElectronTaggerConf.Summer2017Opt_Run1_noVeloCharge
    })


class Summer2017Optimisation_v2_Run2(Summer2017Optimisation):
    """
    New implementation, final iteration
    Mu, K, e are optimised on Run 2 data.
    """
    enabled_taggers_map = deepcopy(Summer2017Optimisation.enabled_taggers_map)
    enabled_taggers_map.update({
        'OSElectronLatest':
        OSElectronTaggerConf.Summer2017Opt_v2_Run2,
        'OSMuonLatest':
        OSMuonTaggerConf.Summer2017Opt_v2_Run2,
        'OSKaonLatest':
        OSKaonTaggerConf.Summer2017Opt_v2_Run2
    })


class Summer2017Optimisation_v2_Run2_noVeloCharge(
        Summer2017Optimisation_v2_Run2):
    """
    New implementation, final iteration
    Mu, K, e are optimised on Run 2 data.
    """
    enabled_taggers_map = deepcopy(
        Summer2017Optimisation_v2_Run2.enabled_taggers_map)
    enabled_taggers_map.update({
        'OSElectronLatest':
        OSElectronTaggerConf.Summer2017Opt_v2_Run2_noVeloCharge,
        'OSMuonLatest':
        OSMuonTaggerConf.Summer2017Opt_v2_Run2,
        'OSKaonLatest':
        OSKaonTaggerConf.Summer2017Opt_v2_Run2
    })


class Summer2017Optimisation_v3_Run2(BaseTuning):
    """
    Mu, K, e are optimised on Run 2 data separately for B2OC and B2CC.
    """
    enabled_taggers_map = {
        "OSCharm":
        OSCharmTaggerConf.Stripping23,
        "OSElectron":
        OSElectronTaggerConf.Stripping23,
        "OSElectronLatestB2OC":
        OSElectronTaggerConf.Summer2017Opt_v1_Run2_Bu2D0pi,
        "OSElectronLatestB2CC":
        OSElectronTaggerConf.Summer2017Opt_v2_Run2,
        "OSKaon":
        OSKaonTaggerConf.Stripping23,
        "OSKaonLatestB2OC":
        OSKaonTaggerConf.Summer2017Opt_v1_Run2_Bu2D0pi,
        "OSKaonLatestB2CC":
        OSKaonTaggerConf.Summer2017Opt_v2_Run2,
        "OSMuon":
        OSMuonTaggerConf.Stripping23,
        "OSMuonLatestB2OC":
        OSMuonTaggerConf.Summer2017Opt_v1_Run2_Bu2D0pi,
        "OSMuonLatestB2CC":
        OSMuonTaggerConf.Summer2017Opt_v2_Run2,
        "OSVtxCh":
        OSVtxChTaggerConf.Stripping23,
        "SSKaon":
        SSKaonTaggerConf.Stripping23NNet,
        "SSKaonLatest":
        SSKaonTaggerConf.Stripping23NNetDev,
        "SSPion":
        SSPionTaggerConf.ClassicBDT,
        "SSProton":
        SSProtonTaggerConf.ClassicBDT,
    }


class Summer2017Optimisation_v4_Run2(BaseTuning):
    """
    Implementation of latest Run2 tunings on B2CC
    (NOMINAL RUN2 TUNINGS)
    """
    enabled_taggers_map = {
        "OSCharm": OSCharmTaggerConf.Stripping23,
        "OSElectron": OSElectronTaggerConf.Stripping23,
        "OSElectronLatest": OSElectronTaggerConf.Summer2017Opt_v2_Run2,
        "OSKaon": OSKaonTaggerConf.Stripping23,
        "OSKaonLatest": OSKaonTaggerConf.Summer2017Opt_v3_Run2,
        "OSMuon": OSMuonTaggerConf.Stripping23,
        "OSMuonLatest": OSMuonTaggerConf.Summer2017Opt_v3_Run2,
        "OSVtxCh": OSVtxChTaggerConf.Stripping23,
        "SSKaon": SSKaonTaggerConf.Stripping23NNet,
        "SSKaonLatest": SSKaonTaggerConf.Stripping23NNetDev,
        "SSPion": SSPionTaggerConf.ClassicBDT,
        "SSProton": SSProtonTaggerConf.ClassicBDT,
    }


class Summer2017OptimisationDev(Summer2017Optimisation_v3_Run2):
    """
    Implementation of latest Run2 tunings (Summer2017Optimisation_v4_Run2)
    Add experimental B2OC-trained taggers
    """
    enabled_taggers_map = {
        "OSCharm":
        OSCharmTaggerConf.Stripping23,
        "OSElectron":
        OSElectronTaggerConf.Stripping23,
        "OSElectronLatestB2OC":
        OSElectronTaggerConf.Summer2017Opt_v1_Run2_Bu2D0pi,
        "OSElectronLatestB2CC":
        OSElectronTaggerConf.Summer2017Opt_v2_Run2,
        "OSKaon":
        OSKaonTaggerConf.Stripping23,
        "OSKaonLatestB2OC":
        OSKaonTaggerConf.Summer2017Opt_v1_Run2_Bu2D0pi,
        "OSKaonLatestB2CC":
        OSKaonTaggerConf.Summer2017Opt_v3_Run2,
        "OSMuon":
        OSMuonTaggerConf.Stripping23,
        "OSMuonLatestB2OC":
        OSMuonTaggerConf.Summer2017Opt_v1_Run2_Bu2D0pi,
        "OSMuonLatestB2CC":
        OSMuonTaggerConf.Summer2017Opt_v3_Run2,
        "OSVtxCh":
        OSVtxChTaggerConf.Stripping23,
        "SSKaon":
        SSKaonTaggerConf.Stripping23NNet,
        "SSKaonLatest":
        SSKaonTaggerConf.Stripping23NNetDev,
        "SSPion":
        SSPionTaggerConf.ClassicBDT,
        "SSProton":
        SSProtonTaggerConf.ClassicBDT,
    }


class Stripping21(Run1):
    pass


class Stripping23(Run1):
    enabled_taggers_map = deepcopy(Run1.enabled_taggers_map)
    enabled_taggers_map.update({
        'OSMuon': OSMuonTaggerConf.Stripping23,
        'OSElectron': OSElectronTaggerConf.Stripping23,
    })


class Development(Run1):
    enabled_taggers_map = {
        'OSMuon': OSMuonTaggerConf.Stripping23,
        'OSMuonDev': OSMuonTaggerConf.Development,
        'OSElectron': OSElectronTaggerConf.Stripping23,
        'OSElectronDev': OSElectronTaggerConf.Development,
        'OSKaon': OSKaonTaggerConf.Stripping21,
        'OSKaonDev': OSKaonTaggerConf.Development,
        'SSPion': SSPionTaggerConf.ClassicBDT,
        'SSPionDev': SSPionTaggerConf.Development,
        'SSProton': SSProtonTaggerConf.ClassicBDT,
        'SSProtonDev': SSProtonTaggerConf.Development,
    }


class TupleDevelopment(Run1):
    enabled_taggers_map = {
        'OSMuonDev': DevelopmentTaggerConf.Dev,
        'OSVtxChDev': OSVtxChTaggerConf.Development,
    }
    preselection = {
        'CutTagPart_MinIPPU': 2,
        'CutTagPart_MinTheta': 0.005,
        'CutTagPart_MinDistPhi': 0.0035,
        'CutTagPart_MaxGhostProb': 0.5,
        'CutTagPart_MinP': 1,
        'CutTagPart_MaxP': 500,
        'CutTagPart_MaxPT': 20,
        'CutTagPart_MinCloneDist': 5000,
    }


def applyTuning(tagging_tool, tuning_version='Summer2017Optimisation_v4_Run2'):
    """
    Apply a tuning version to the given TaggingTool instance.

    Each tuning instance consists of a combination of specific tagger +
    preselection combinations which are defined within indpendent python
    files.

    :param tagging_tool: the TaggingTool instance to be tuned
    :param tuning_version: the tuning version, one of the `tunings` dictionary
        within `FlavourTagging.Tunings`
    :return: the tuned TaggingTool
    """

    def allTunings(base=BaseTuning):
        """Return a list of all tunings aka. all subclasses of `base`"""
        subclasses = set()
        work = [base]
        while work:
            parent = work.pop()
            for child in parent.__subclasses__():
                if child not in subclasses:
                    subclasses.add(child.__name__)
                    work.append(child)
        print(subclasses)
        return subclasses

    current_tuning = globals().get(tuning_version)
    if not current_tuning or not issubclass(current_tuning, BaseTuning):
        print('Flavour Tagging tuning version `{}` not found!\n\t'
              'Available versions are {}.\n\t'
              'Please check python/FlavourTagging/Tunings.py for details.\n\t'
              'Exiting now.'.format(tuning_version, allTunings()))
        sys.exit()
    else:
        print('Using FlavourTagging tuning `%s`.' % tuning_version)

    # set enabled taggers to the BTaggingTool
    tagging_tool.EnabledTaggersMap = {}
    for key, tagger in current_tuning.enabled_taggers_map.items():
        tagging_tool.addTool(tagger)
        tagging_tool.EnabledTaggersMap[key] = tagger.getFullName()

    # @TODO Add the preselection tool
    #tagging_tool.addTool(current_tuning.preselection)
    tagging_tool.CallOrder = current_tuning.call_order

    for param, value in current_tuning.preselection.items():
        print(type(tagging_tool))
        if param in tagging_tool.getProperties():
            setattr(tagging_tool, param, value)
        else:
            print('Trying to set property `{}` of tagging tool `{}`, but the '
                  'tool has no such property. Please check the `preselection` '
                  'parameter of the currently active tuning `{}`.'.format(
                      param, tagging_tool.getFullName(),
                      type(current_tuning).__name__))
            sys.exit()
    return tagging_tool


def TuneTool(tuple_tool_tagging, version, fttoolname='BTaggingTool'):
    """
    BACKWARDS COMPATIBILITY FUNCTION. Use `applyTuning` directly with a
    BTaggingTool instance instead.
    A *new* BTaggingTool instance with name `fttoolname` will be added to the
    given TupleToolTagging instance.

    :param tuple_tool_tagging: the TupleToolTagging instance to be tuned
    :param version: the tuning version to be applied
    :fttoolname: name of the new BTaggingTool
    :returns: the tuned tuple_tool_tagging
    """
    btagtool = tuple_tool_tagging.addTool(BTaggingTool, name=fttoolname)
    applyTuning(btagtool, tuning_version=version)
    return tuple_tool_tagging
