###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    DevelopmentTagger, )

Dev = DevelopmentTagger(name='DevelopmentTagger_Dev')

Dev.Aliases = {
    'BPVIPCHI2': 'BPVIPCHI2()',  # prevent root tuple errors
    'PVndof': 'BPV(VDOF)',
}

Dev.SelectionPipeline = [
    [
        'PT',  # order by first feature
        'ABSID',  # duplicate features, but meh
        'ID',  # ...
        'Q',  # ...
        'AbsIP',
        'BPVIPCHI2',
        'IPErr',
        'IPPUSig',
        'IPSig',
        'IsSignalDaughter',
        'MuonPIDIsMuon',
        'P',
        'PIDK',
        'PIDp',
        'PIDe',
        'PIDmu',
        'PIDp',
        'PP_InAccHcal',
        'PP_VeloCharge',
        # these PROBNNs will use loki functors, which will essentially read
        # the MC12TuneV2
        'PROBNNe',
        'PROBNNk',
        'PROBNNmu',
        'PROBNNp',
        'PROBNNpi',
        # these PROBNNs are manually tuned within FeatureGenerator
        'PROBNNe_MC12TuneV4',
        'PROBNNk_MC12TuneV4',
        'PROBNNmu_MC12TuneV4',
        'PROBNNp_MC12TuneV4',
        'PROBNNpi_MC12TuneV4',
        'PROBNNghost_MC12TuneV4',
        'PROBNNe_MC15TuneV1',
        'PROBNNk_MC15TuneV1',
        'PROBNNmu_MC15TuneV1',
        'PROBNNp_MC15TuneV1',
        'PROBNNpi_MC15TuneV1',
        'PROBNNghost_MC15TuneV1',
        'PVndof',
        'Signal_TagPart_PT',
        'Signal_TagPart_CHI2NDOF',
        'TRCHI2DOF',
        'TRGHP',
        'TRLH',
        'TRTYPE',
        'DeltaQ',
        'DeltaR',
        'etaDistance',
        'phiDistance',
        'eOverP',
        'minPhiDistance',
        'countTracks',
        'nPV',
        'Signal_PT',
    ],
]
