/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COMBINETAGGERSNN_H
#define COMBINETAGGERSNN_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"
#include "ICombineTaggersTool.h" // Interface

// nnet
#include "src/NeuralNet/NNcomb.cxx"

/** @class CombineTaggersNN CombineTaggersNN.h
 *
 *
 *  @author Marc Grabalosa Gandara
 *  @date   2010-10-13
 */

class CombineTaggersNN : public GaudiTool, virtual public ICombineTaggersTool {

public:
  /// Standard constructor
  CombineTaggersNN( const std::string& type, const std::string& name, const IInterface* parent );

  ~CombineTaggersNN(); ///< Destructor

  int combineTaggers( LHCb::FlavourTag& theTag, std::vector<LHCb::Tagger*>&, int, bool, bool ) override;

private:
  double m_P0_NN, m_P1_NN, m_P2_NN, m_ProbMin;
  float  pmu, pe, pk, pss, pvtx;
};
#endif // COMBINETAGGERSNN_H
