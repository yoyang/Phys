/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TaggingClassifierXGB.h"

TaggingClassifierXGB::TaggingClassifierXGB() {
  // XGDMatrixCreateFromMat(
  //     &m_predictionsCache.at(0),
  //     1,
  //     2,
  //     0.5,
  //     &m_cache_matrix);
}

void TaggingClassifierXGB::setPath( const std::string& ) {
  // XGBoosterCreate(&m_cache_matrix, 1, &m_booster);
  // XGBoosterLoadModel(m_booster, path.c_str());
}

double TaggingClassifierXGB::getClassifierValue( const std::vector<double>& ) {
  // // currently XGBoost only supports float
  // std::vector<float> features_f(featureValues.begin(), featureValues.end());

  // // fill the feature vector into a XGBoost DMatrix
  // XGDMatrixCreateFromMat(&features_f.at(0),
  //                        1,
  //                        features_f.size(),
  //                        0,
  //                        &m_feature_matrix);

  // // XGBoost returns the predictions into a arrayish object and return
  // // its size
  // unsigned long predictions_length;
  // const float *predictions;
  // XGBoosterPredict(m_booster,
  //                  m_feature_matrix,
  //                  0,
  //                  0,
  //                  &predictions_length,
  //                  &predictions);

  // if(predictions_length == 1)
  //   return *predictions;
  // else
  return 0.5;
}
