/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_ITAGGINGCLASSIFIERFACTORY_H
#define PHYS_PHYS_FLAVOURTAGGING_ITAGGINGCLASSIFIERFACTORY_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// local
#include "ITaggingClassifier.h"

class ITaggingClassifierFactory : virtual public extend_interfaces<IAlgTool> {
public:
  DeclareInterfaceID( ITaggingClassifierFactory, 1, 0 );

  virtual StatusCode initialize() override = 0;

  virtual std::unique_ptr<ITaggingClassifier> taggingClassifier()                                                   = 0;
  virtual std::unique_ptr<ITaggingClassifier> taggingClassifier( const std::string& type, const std::string& name ) = 0;
};

#endif // PHYS_PHYS_FLAVOURTAGGING_ITAGGINGCLASSIFIERFACTORY_H
