/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <memory>

#include "GaudiAlg/GaudiTool.h"

#include "boost/log/core.hpp"
#include "boost/log/expressions.hpp"
#include "boost/log/trivial.hpp"
#include "boost/range/combine.hpp"

#include "src/Features/FeatureGenerator.h"
#include "src/Selection/SelectionParser.h"

using namespace std;

/** @brief A pipeline can be configured to contain multiple vectors of
 * selection steps, which are applied to a given event configuration.
 *
 * The event is defined by a unique set of
 * 1. A signal particle candidate
 * 2. A reconstruction vertex
 * 3. A vector of tagging particles
 * 4. A vector of pile up vertices
 *
 * The pipeline will apply the selections to one or multiple given tagging
 * particle candidates. It can return a vector of vectors of doubles,
 * containing the features that were used for selection.
 * The returned vector will be sorted by a given feature (default: the first
 * feature in the selection pipeline).
 *
 * LoKi functors can be used as features and Flavour Tagging specific features
 * are defined within the FeatureGenerator class.
 * Different MVAs can also be applied as features.
 */
class Pipeline {
public:
  Pipeline();

  /** @brief Set the tool provider which will be the parent of all GaudiTools
   * that might be needed for feature calculations.
   * @param[in] provider a GaudiTool instance, e.g. a tagger.
   */
  void setToolProvider( const GaudiTool* provider );

  /** @brief Define aliases for features.
   * @param[in] aliasMap
   *   a map of the form `{"alias": "someComplexFeaturesName"}`
   */
  void setAliases( const map<string, string>& aliasMap );

  /** @brief define the selection pipeline.
   * @param[in] selectionPipeline
   *   a vector of vector of strings containing features and selections.
   */
  void setPipeline( const vector<vector<string>>& selectionPipeline );

  /** @brief define the feature which is used for sorting.
   * This feature needs to be defined somewhere in the selection pipeline.
   * @param[in] featureName the name of the sorting feature.
   */
  void setSortingFeature( const string& featureName );

  /** @brief update the signal candidate.
   * This will invalidate all features, as this changes the current event.
   * @param[in] signalCandidate the new signal candidate.
   */
  void setSignalCandidate( const LHCb::Particle* signalCandidate );

  /** @brief the associated vertex is used to calculate several Flavour
   * Tagging specific features.
   * See the feature definitions inside FeatureGenerator for more details.
   * This will invalidate all features and define a new event.
   * @param[in] associatedVertex the associated vertex.
   */
  void setReconstructionVertex( const LHCb::RecVertex* associatedVertex );

  /** @brief update the vector of tagging particles defining the current
   * event.
   * This will invalidate all features and define a new event.
   * @param[in] taggingParticles a vector of tagging particles.
   */
  void setTaggingParticles( const vector<const LHCb::Particle*>& taggingParticles );

  /** @brief update the vector of pile up vertices used for Flavour Tagging
   * specific feature calculation.
   * This will invalidate all features and define a new event.
   * @param[in] pileUpVertices a vector of pile up vertices.
   */
  void setPileUpVertices( const vector<const LHCb::RecVertex*>& pileUpVertices );

  /** @brief manually update the current tagging particle.
   * This will NOT update the event, but calling features() or
   * mergedFeatures() will now return the feature vectors which correspond to
   * the given particle.
   * @param[in] particle the current tagging particle.
   */
  void setTaggingParticle( const LHCb::Particle* particle );

  /** @brief apply the configured selection to a set of tagging particle
   * candidates.
   * @param[in] candidates vector of tagging particle candidates.
   * @return the vector of tagging particles, which pass the selection,
   *   ordered by m_sortFeatureName.
   */
  vector<const LHCb::Particle*> applySelection( const vector<const LHCb::Particle*>& candidates );

  /** @brief receive N selected tagging particles, ordered by m_sortFeatureName.
   * @param[in] candidates vector of taggin particle candidates.
   * @param[in] n number of particles in vector to be returned. default=1.
   * @return a sorted vector of n selected tagging particles.
   */
  vector<const LHCb::Particle*> selectN( const vector<const LHCb::Particle*>& candidates, const size_t n = 1 );

  /** @brief receive a single selected tagging particle, chosen by m_sortFeatureName.
   * @param[in] candidates vector of tagging partiles to chose from.
   * @returns a single selected tagging particle.
   */
  const LHCb::Particle* selectOne( const vector<const LHCb::Particle*>& candidates );

  /** @brief check whether the given particle will pass the selection
   * pipeline.
   * Will update the current tagging particle, but NOT change the current
   * event.
   * @param[in] candidate particle to check.
   * @return wheter the given particle will pass the selections.
   */
  bool applySelection( const LHCb::Particle* candidate );

  /** @brief receive the feature pipeline vectors of the current tagging
   * particle, corresponding to what is defined via setPipeline.
   * @return current feature values.
   */
  vector<vector<double>> features();

  /** @brief receive the feature pipeline vectors of the given particle.
   * @param[in] candidate particle for which the features will be calculated.
   * @return feature values for given particle.
   */
  vector<vector<double>> features( const LHCb::Particle* candidate );

  /** @brief receive all feature names that are computed within the pipeline.
   * @return vector of vector of feature names.
   */
  vector<vector<string>> featureNames();

  /** @brief receive a vector of feature values for all unique features.
   * This will essentially join the single pipeline elements.
   * A pipeline [['pt', 'mva'], ['pt', 'IPPU']] will result in a vector
   * containing values for ['pt', 'mva', 'IPPU'].
   * @return values of all unique features.
   */
  vector<double> mergedFeatures();

  /** @brief receive a matrix of merged features for all tagging particles in
   * the current event.
   * @return vector of merged feature vectors for the current event.
   */
  vector<vector<double>> mergedFeatureMatrix();

  /** @breif receive the names of unique features within the configured
   * pipeline.
   * @return vector of unique, merged feature names.
   */
  vector<string> mergedFeatureNames();

  /** @brief receive the mva value of the last mva feature within the
   * pipeline, for a given (selected) tagging particle.
   *
   * Currently, calling this function with a taggingParticle that has not
   * been chosen via one of the selection functions (applySelection,
   * selectOne, selectN) might fill the cache with features from unselected
   * particles.
   *
   * @param[in] taggingParticle the particle whose mva value should be
   *   returned. -1 if the particle has not been found in the selected
   *   tagging particles of the current events.
   */
  double getLastMVAValue( const LHCb::Particle* taggingParticle );

private:
  /** @brief initialize and add several GaudTools to the given generator.
   * This raises an exception if no toolprovidor is set.
   */
  void addToolsToGenerator( FeatureGenerator* generator ) const;

  /**
   * Calculate union of all features in the current pipeline.
   * This function will update m_mergedFeaturesIndices such that the correct
   * featureName - featureValue pairs can be produced.
   */
  void mergePipeline();

  /**
   * within the given features vector, update feature values that have been
   * calculated in previous pipeline elements. The links are figured out in
   * the filterCommonFeatures funciton.
   */
  void applyLinks( vector<double>& features, const unordered_map<size_t, pair<size_t, size_t>>& links );

  /** Synchronize output level between boost::logging and Gaudi::logging
   * Uses the m_toolProvider's OutputLevel property if available.
   * Sets level to `boost::log::trivial::severity_level::fatal` otherwise.
   */
  void syncOutputLevel();

  /** Add a single element to the selection pipeline by creating and storing
   * a pair of FeatureGenerator and SelectionParser with the given selection
   * vector
   */
  void addPipelineElement( const vector<string>& element );

  // the vector of all merged feature names. Updated via mergePipeline.
  vector<string> m_mergedFeatureNames;
  // vector of (pipeline, feature-in-pipeline) indices used to merge the
  // pipeline features.
  vector<pair<size_t, size_t>> m_mergedFeatureIndices;

  // definition of a pipeline element
  // @TODO maybe use struct here?
  using FG              = unique_ptr<FeatureGenerator>;
  using SP              = unique_ptr<SelectionParser>;
  using LINK            = unordered_map<size_t, pair<size_t, size_t>>;
  using CACHE           = vector<double>;
  using STATE           = bool;
  using PipelineElement = tuple<FG, SP, LINK, CACHE, STATE>;

  /** @brief pipeline entries, containing a set of
   *   - FeatureGenerator (transforms eventinfo into vector of floats)
   *   - SelectionParser (applies selections to vectors)
   *   - index-map, storing information of previously processed features
   *   - some memory to cache the feature values
   *   - info whether the features are up to date
   */
  vector<PipelineElement> m_pipeline;

  const GaudiTool* m_toolProvider = nullptr;

  /**
   * A helper function for pretty printing
   */
  string featureLog( vector<string> names, vector<double> values );

  /**
   * Find common features of given feature vector inside previous pipeline
   * elements
   */
  unordered_map<size_t, pair<size_t, size_t>> filterCommonFeatures( const vector<string>& e );

  // vector of tagging classifiers to keep these alive
  vector<const ITaggingClassifier*> m_mvaTools = {};
  map<string, string>               m_aliasMap = {{}};
  const LHCb::Particle*             m_taggingParticle;

  // the index of the sorting feature within the mergedFeatureVector
  size_t m_sortMergedIndex;
  string m_sortFeatureName = "";

  /**
   * Low level function to actually call the FeatureGenerator of every
   * pipeline element.
   * This function will also apply all feature links between pipeline element
   * feature values.
   */
  vector<double>& generatePipelineElementFeatures( PipelineElement& e );

  void calculateAndCheckFeatures( PipelineElement& e, bool& passing );

  /** @brief access map for given particle and vice versa
   */
  unordered_map<const LHCb::Particle*, shared_ptr<vector<double>>> m_mergedParticleCacheMap        = {{}};
  unordered_map<shared_ptr<vector<double>>, const LHCb::Particle*> m_mergedParticleCacheReverseMap = {{}};

  // the merged feature cache
  vector<shared_ptr<vector<double>>> m_mergedFeatureCache = {};

  /**
   * this function should be called whenever the event definition is updated,
   * i.e. the signal candidate, reconstruction vertex or tagging particle
   * vector is changed.
   */
  void resetCache() noexcept;

  // store index within merged features of last mva value in the pipeline
  size_t m_lastMVAFeature = 0;
};
