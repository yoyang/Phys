/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from STL
#include <algorithm>
#include <cstdio>
#include <fstream>
#include <math.h>
#include <string>

// Boost
#include <boost/foreach.hpp>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"

// from Event
#include "Event/FlavourTag.h"
#include "Event/RecHeader.h"
#include "Event/Track.h"

// Kernels
#include "Kernel/IBTaggingTool.h"
#include "Kernel/IPVReFitter.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IRelatedPVFinder.h"
#include "Kernel/ITagger.h"
#include <Kernel/GetIDVAlgorithm.h>
#include <Kernel/IDVAlgorithm.h>

// local
#include "Combination/ICombineTaggersTool.h"
#include "FlavourTagging/ITaggingUtils.h"
#include "src/Features/FeatureGeneratorCache.h"
#include "src/Utils/TaggingHelpers.h"

// class DaVinciAlgorithm;

/** @class BTaggingTool BTaggingTool.h
 *
 *  Tool to tag the B flavour
 *
 *  @author Marco Musy
 *  @date   05/06/2005
 */

class BTaggingTool : public GaudiTool, virtual public IBTaggingTool {

public:
  /// Standard constructor
  BTaggingTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override; ///< initialization

  //-------------------------------------------------------------
  StatusCode tag( LHCb::FlavourTag& flavTag, const LHCb::Particle* sigPart ) override;

  StatusCode tag( LHCb::FlavourTag& flavTag, const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx ) override;

  StatusCode tag( LHCb::FlavourTag& flavTag, const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx,
                  LHCb::Particle::ConstVector& tagParts ) override;

  std::vector<std::string> activeTaggerTypeNames() const override;

  std::vector<LHCb::Tagger::TaggerType> activeTaggerTypes() const override;

  std::vector<std::string> featureNames( LHCb::Tagger::TaggerType taggerType ) const override;

  std::vector<double> featureValues( LHCb::Tagger::TaggerType taggerType ) const override;

  std::vector<std::string> featureNamesTagParts( LHCb::Tagger::TaggerType taggerType ) const override;

  std::vector<std::vector<double>> featureValuesTagParts( LHCb::Tagger::TaggerType taggerType ) const override;

private:
  const LHCb::RecVertex::ConstVector choosePrimary( const LHCb::Particle*         sigPart,
                                                    const LHCb::RecVertex::Range& allVtxs,
                                                    const LHCb::RecVertex*& assocVtx, LHCb::RecVertex& assocVtxRefit );

  const LHCb::Particle::ConstVector chooseCandidates( const LHCb::Particle*               sigPart,
                                                      const LHCb::Particle::Range&        allTagParts,
                                                      const LHCb::RecVertex::ConstVector& puVtxs );

  const LHCb::Particle::ConstVector chooseCandidatesReco14( const LHCb::Particle*               sigPart,
                                                            const LHCb::Particle::Range&        allTagParts,
                                                            const LHCb::RecVertex::ConstVector& puVtxs );

  void clearExtraInfo();

  Gaudi::Property<std::map<std::string, std::string>> m_enabledTaggersMap{
      this,
      "EnabledTaggersMap",
      {{"OSMuon", "OSMuonTagger"},
       {"OSElectron", "OSElectronTagger"},
       {"OSKaon", "OSKaonTagger"},
       {"OSVtxCh", "OSVertexChargeTagger"},
       {"OSCharm", "OSCharmTagger"},
       {"SSPion", "SSPionTagger"},
       {"SSProton", "SSProtonTagger"},
       {"SSKaon", "SSKaonTagger"}},
      "Map of target TaggerTypes and names of the associated ITagger instances."};

  Gaudi::Property<std::vector<std::string>> m_enabledTaggersCallOrder{
      this,
      "CallOrder",
      {"OSMuon", "OSKaon", "OSElectron", "SSKaon", "SSPion", "OSVtxCh", "SSProton", "OSCharm"},
      "Order in which taggers should be called, represented by their TaggerType. "
      "Taggers that are not shared between the CallOrder vector and the "
      "EnabledTaggersMap will be appended in their alphabetic order."};

  Gaudi::Property<std::string> m_tagPartLoc{this, "TaggingParticleLocation", "Phys/TaggingParticles",
                                            "TES location of the tagging particles."};

  Gaudi::Property<std::string> m_assocPVCriterion{
      this, "AssocPVCriterion", "bestPV", "Criterion to choose the associated PV. Choices are 'bestPV', 'PVbyIPs'"};

  Gaudi::Property<bool> m_usePVRefit{this, "UsePVRefit", true, "Apply a refit to the associated PV."};

  Gaudi::Property<bool> m_vetoFailedPVRefits{this, "VetoFailedPVRefits", false,
                                             "Do not consider associated PV if PV refit fails"};

  Gaudi::Property<double> m_cutTagPart_MinIPPU{this, "CutTagPart_MinIPPU", 3.0,
                                               "Tagging particle requirement: Minimum IP to all pile-up vertices"};

  Gaudi::Property<double> m_cutTagPart_MinTheta{this, "CutTagPart_MinTheta", 0.012,
                                                "Tagging particle requirement: Minimum theta angle"};

  Gaudi::Property<double> m_cutTagPart_MinDistPhi{this, "CutTagPart_MinDistPhi", 0.005,
                                                  "Tagging particle requirement: Minimum phi distance"};

  Gaudi::Property<double> m_cutTagPart_MaxGhostProb{this, "CutTagPart_MaxGhostProb", 0.5,
                                                    "Tagging particle requirement: Maximum ghost probability"};

  Gaudi::Property<double> m_cutTagPart_MinP{this, "CutTagPart_MinP", 2,
                                            "Tagging particle requirement: Minimum P (in GeV)"};

  Gaudi::Property<double> m_cutTagPart_MaxP{this, "CutTagPart_MaxP", 200,
                                            "Tagging particle requirement: Maximum P (in GeV)"};

  Gaudi::Property<double> m_cutTagPart_MaxPT{this, "CutTagPart_MaxPT", 10,
                                             "Tagging particle requirement: Maximum PT (in GeV)"};

  Gaudi::Property<double> m_cutTagPart_MinCloneDist{this, "CutTagPart_MinCloneDist", 5000,
                                                    "Tagging particle requirement: Minimum Track::CloneDist"};

  std::vector<std::pair<LHCb::Tagger::TaggerType, ITagger*>> m_activeTaggers;

  ITagger* getActiveTagger( LHCb::Tagger::TaggerType taggerType ) const {
    auto activeTagger = std::find_if( m_activeTaggers.begin(), m_activeTaggers.end(),
                                      [&taggerType]( const auto& element ) { return element.first == taggerType; } );

    if ( activeTagger != m_activeTaggers.end() ) {
      return activeTagger->second;
    } else {
      return nullptr;
    }
  }

  ITaggingUtils*        m_taggingUtils = nullptr;
  IParticleDescendants* m_partDescends = nullptr;
  const IPVReFitter*    m_pvReFitter   = nullptr;

  IDVAlgorithm* m_parentDVA = nullptr;

  std::vector<LHCb::Particle*> m_extraInfoToClear; ///< Particles to clear extra info from

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nCandsCounter{this, "nCands"};
};
