/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_TAGGINGUTILS_H
#define PHYS_PHYS_FLAVOURTAGGING_TAGGINGUTILS_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IDVAlgorithm.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IPVReFitter.h"
// from Event

#include "FlavourTagging/ITaggingUtils.h"

#include "Kernel/IParticleDescendants.h"

/** @class TaggingUtils TaggingUtils.h
 *
 *  Tool to tag the B flavour with a Electron Tagger
 *
 *  @author Marco Musy
 *  @date   30/06/2005
 */

class TaggingUtils : public GaudiTool, virtual public ITaggingUtils {

public:
  /// Standard constructor
  TaggingUtils( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TaggingUtils();
  StatusCode initialize() override;

  //----------------------------------------------------------------------------
  // acessors to fitters
  const IPVReFitter*         getPVReFitter() const override { return m_PVReFitter; }
  const ILifetimeFitter*     getLifetimeFitter() const override { return m_LifetimeFitter; }
  const IVertexFit*          getVertexFitter() const override { return m_VertexFitter; }
  const IDistanceCalculator* getDistanceCalculator() const override { return m_DistanceCalculator; }
  IParticleDescendants*      getParticleDescendants() const override { return m_ParticleDescendants; }

  //-------------------------------------------------------------
  StatusCode calcIP( const LHCb::Particle* axp, const LHCb::VertexBase* v, double& ip, double& iperr ) override;

  StatusCode calcIP( const LHCb::Particle*, const LHCb::RecVertex::ConstVector&, double&, double& ) override;

  StatusCode GetVCHI2NDOF( const LHCb::Particle* sigp, const LHCb::Particle* tagp, double& VCHI2NDOF ) override;

  StatusCode calcDOCAmin( const LHCb::Particle* axp, const LHCb::Particle* p1, const LHCb::Particle* p2, double& doca,
                          double& docaerr ) override;

  int countTracks( const LHCb::Particle::ConstVector& ) override;

  static bool qualitySort( const LHCb::Particle* p, const LHCb::Particle* q );

  bool isInTree( const LHCb::Particle* B0Candidate, const LHCb::Particle::ConstVector& daughterCandidates ) override;

  bool isInTree( const LHCb::Particle* B0Candidate, const LHCb::Particle::ConstVector& daughterCandidates,
                 double& distPhi ) override;

  double TPVTAU( const LHCb::Particle*, const LHCb::RecVertex* ) override;
  double TPVDIRA( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) override;
  double TPVFD( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) override;
  double TPVFDCHI2( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) override;
  double TPVIPCHI2( const LHCb::Particle* cand, const LHCb::RecVertex* vert, const char* id = nullptr ) override;
  bool   isBestPV( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) override;

  // classify charm decay modes
  CharmTaggerSpace::CharmMode getCharmDecayMode( const LHCb::Particle*, int ) override;

  // remove any charm cand that has descendents in common with the signal B
  LHCb::Particle::ConstVector purgeCands( const LHCb::Particle::Range& cands, const LHCb::Particle& BS ) override;

  //-------------------------------------------------------------

private:
  IDVAlgorithm* m_dva;

  std::string m_PVSelCriterion;

  std::string m_algNamePVReFitter;
  std::string m_algNameLifetimeFitter;
  std::string m_algNameVertexFitter;
  std::string m_algNameDistanceCalculator;
  std::string m_algNameParticleDescendants;

  const IPVReFitter*         m_PVReFitter;
  const ILifetimeFitter*     m_LifetimeFitter;
  const IVertexFit*          m_VertexFitter;
  const IDistanceCalculator* m_DistanceCalculator;
  IParticleDescendants*      m_ParticleDescendants; // cannot be const, as
                                                    // descendants() is not a const
                                                    // function.

  unsigned int lambda_pid, pi_pid, pi0_pid, k_pid, ks_pid, p_pid, e_pid, mu_pid, d0_pid, d_pid, lambdaC_pid;
};

//===============================================================//
#endif // PHYS_PHYS_FLAVOURTAGGING_TAGGINGUTILS_H
