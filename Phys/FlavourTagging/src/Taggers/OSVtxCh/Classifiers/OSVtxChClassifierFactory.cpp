/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "OSVtxChClassifierFactory.h"

// ITaggingClassifier implementations
#include "TMVA/OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( OSVtxChClassifierFactory )

OSVtxChClassifierFactory::OSVtxChClassifierFactory( const std::string& type, const std::string& name,
                                                    const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITaggingClassifierFactory>( this );
}

StatusCode OSVtxChClassifierFactory::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  addTMVAClassifier<OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0>( "OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0" );

  return StatusCode::SUCCESS;
}

std::unique_ptr<ITaggingClassifier> OSVtxChClassifierFactory::taggingClassifier() {
  return m_classifierMapTMVA[m_classifierName]();
}

std::unique_ptr<ITaggingClassifier> OSVtxChClassifierFactory::taggingClassifier( const std::string& type,
                                                                                 const std::string& name ) {
  if ( type == "TMVA" ) {
    return m_classifierMapTMVA[name]();
  } else {
    error() << "Classifier of type " << type << " unknown!" << endmsg;
    return nullptr;
  }
}
