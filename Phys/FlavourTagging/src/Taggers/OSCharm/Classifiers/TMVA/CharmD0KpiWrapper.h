/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHARMD0KPIWRAPPER_H
#define CHARMD0KPIWRAPPER_H 1

// Include files

// local
#include "src/TMVAWrapper.h"

namespace MyD0KpiSpace {
  class ReadBDT;
  class PurityTable;
} // namespace MyD0KpiSpace

/** @class CharmD0KpiWrapper CharmD0KpiWrapper.h
 *
 *  Wrapper for D0 -> Kpi classifier
 *
 *  @author Jack Timpson Wimberley
 *  @date   2014-02-18
 */
class CharmD0KpiWrapper : public TMVAWrapper {
public:
  CharmD0KpiWrapper( std::vector<std::string>& );
  ~CharmD0KpiWrapper();
  double GetMvaValue( std::vector<double> const& ) override;

protected:
private:
  MyD0KpiSpace::ReadBDT*     mcreader;
  MyD0KpiSpace::PurityTable* purtable;
};
#endif // CHARMD0KPIWRAPPER_H
