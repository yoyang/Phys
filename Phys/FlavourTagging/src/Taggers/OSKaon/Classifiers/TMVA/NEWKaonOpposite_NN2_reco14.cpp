/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "NEWKaonOpposite_NN2_reco14.h"

// hack, otherwise: redefinitions...
//
namespace MyDataSpace_reco14 {
#include "weights/NN2_OSK_reco14.dat/TMVAClassification_MLPBNN.class.C"
}

DataReaderCompileWrapper_reco14::DataReaderCompileWrapper_reco14( std::vector<std::string>& names )
    : datareader_reco14( new MyDataSpace_reco14::ReadMLPBNN( names ) ) {}

DataReaderCompileWrapper_reco14::~DataReaderCompileWrapper_reco14() { delete datareader_reco14; }

double DataReaderCompileWrapper_reco14::GetMvaValue( std::vector<double> const& values ) {
  return datareader_reco14->GetMvaValue( values );
}
