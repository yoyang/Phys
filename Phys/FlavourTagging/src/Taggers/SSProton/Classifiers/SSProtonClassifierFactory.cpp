/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SSProtonClassifierFactory.h"

// ITaggingClassifier implementations
// #include "TMVA/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0.h"
#include "TMVA/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SSProtonClassifierFactory )

SSProtonClassifierFactory::SSProtonClassifierFactory( const std::string& type, const std::string& name,
                                                      const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITaggingClassifierFactory>( this );
}

StatusCode SSProtonClassifierFactory::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  // This tagger should not be used
  // addTMVAClassifier<SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0>("SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0");
  addTMVAClassifier<SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1>( "SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1" );

  return StatusCode::SUCCESS;
}

std::unique_ptr<ITaggingClassifier> SSProtonClassifierFactory::taggingClassifier() {
  return m_classifierMapTMVA[m_classifierName]();
}

std::unique_ptr<ITaggingClassifier> SSProtonClassifierFactory::taggingClassifier( const std::string& type,
                                                                                  const std::string& name ) {
  if ( type == "TMVA" ) {
    return m_classifierMapTMVA[name]();
  } else {
    error() << "Classifier of type " << type << " unknown!" << endmsg;
    return nullptr;
  }
}
