/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SSPROTON_CLASSIFIER_DATA_RUN1_ALL_BD2DPI_TMVA_BDT_v1r0_H
#define SSPROTON_CLASSIFIER_DATA_RUN1_ALL_BD2DPI_TMVA_BDT_v1r0_H 1

#include "src/Classification/TaggingClassifierTMVA.h"

#define NN new ssProtonNode

#ifndef ssProtonNode__def
#  define ssProtonNode__def

class ssProtonNode {

public:
  // constructor of an essentially "empty" node floating in space
  ssProtonNode( ssProtonNode* left, ssProtonNode* right, int selector, double cutValue, bool cutType, int nodeType,
                double purity, double response )
      : fLeft( left )
      , fRight( right )
      , fSelector( selector )
      , fCutValue( cutValue )
      , fCutType( cutType )
      , fNodeType( nodeType )
      , fPurity( purity )
      , fResponse( response ) {}

  virtual ~ssProtonNode() {
    if ( fLeft != NULL ) delete fLeft;
    if ( fRight != NULL ) delete fRight;
  }

  // test event if it decends the tree at this node to the right
  virtual bool GoesRight( const std::vector<double>& inputValues ) const {
    // test event if it decends the tree at this node to the right
    bool result = ( inputValues[fSelector] > fCutValue );
    if ( fCutType == true )
      return result; // the cuts are selecting Signal ;
    else
      return !result;
  }

  ssProtonNode* GetRight( void ) { return fRight; };

  // test event if it decends the tree at this node to the left
  virtual bool GoesLeft( const std::vector<double>& inputValues ) const {
    // test event if it decends the tree at this node to the left
    if ( !this->GoesRight( inputValues ) )
      return true;
    else
      return false;
  }

  ssProtonNode* GetLeft( void ) { return fLeft; };

  // return  S/(S+B) (purity) at this node (from  training)

  double GetPurity( void ) const { return fPurity; }
  // return the node type
  int    GetNodeType( void ) const { return fNodeType; }
  double GetResponse( void ) const { return fResponse; }

private:
  ssProtonNode* fLeft;     // pointer to the left daughter node
  ssProtonNode* fRight;    // pointer to the right daughter node
  int           fSelector; // index of variable used in node selection (decision tree)
  double        fCutValue; // cut value appplied on this node to discriminate bkg against sig
  bool          fCutType;  // true: if event variable > cutValue ==> signal , false otherwise
  int           fNodeType; // Type of node: -1 == Bkg-leaf, 1 == Signal-leaf, 0 = internal
  double        fPurity;   // Purity of node from training
  double        fResponse; // Regression response value of node
};

#endif

class SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0 : public TaggingClassifierTMVA {

public:
  SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0();
  virtual ~SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0();

  double GetMvaValue( const std::vector<double>& featureValues ) const override;

private:
  // method-specific destructor
  void Clear();

  // common member variables
  const char* fClassName = "SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0";

  const size_t fNvars = 9;
  size_t       GetNvar() const { return fNvars; }
  char         GetType( int ivar ) const { return fType[ivar]; }

  // normalisation of input variables
  const bool fIsNormalised = false;
  bool       IsNormalised() const { return fIsNormalised; }
  double     fVmin[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
  double     fVmax[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

  double NormVariable( double x, double xmin, double xmax ) const {
    // normalise to output range: [-1, 1]
    return 2 * ( x - xmin ) / ( xmax - xmin ) - 1.0;
  }

  // type of input variable: 'F' or 'I'
  char fType[9] = {'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F'};

  // initialize internal variables
  void   Initialize();
  double GetMvaValue__( const std::vector<double>& inputValues ) const;

  // private members (method specific)
  std::vector<ssProtonNode*> fForest;       // i.e. root nodes of decision trees
  std::vector<double>        fBoostWeights; // the weights applied in the individual boosts
};

#endif // SSPROTON_CLASSIFIER_DATA_RUN1_ALL_BD2DPI_TMVA_BDT_v1r0_H
