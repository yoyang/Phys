/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SSKaon_BDTseltracks_dev.h"

// hack, otherwise: redefinitions...
//
namespace MyBDTseltracksSpace {
#include "weights/BDTseltracks_SSK_dev/TMVAClassification_BDTG_selBestTracks_14vars.class.C"

}

BDTseltracksReaderCompileWrapper::BDTseltracksReaderCompileWrapper( std::vector<std::string>& names )
    : BDTseltracksreader( new MyBDTseltracksSpace::ReadBDTG_selBestTracks_14vars( names ) ) {}

BDTseltracksReaderCompileWrapper::~BDTseltracksReaderCompileWrapper() { delete BDTseltracksreader; }

double BDTseltracksReaderCompileWrapper::GetMvaValue( std::vector<double> const& values ) {
  return BDTseltracksreader->GetMvaValue( values );
}
