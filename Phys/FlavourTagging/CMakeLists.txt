###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: FlavourTagging
################################################################################
gaudi_subdir(FlavourTagging)

gaudi_depends_on_subdirs(Calo/CaloInterfaces
                         Event/PhysEvent
                         GaudiPython
                         Phys/DaVinciKernel
                         Phys/LoKiPhys
                         Phys/LoKiArrayFunctors
                         PhysSel/PhysSelPython
                         Tr/TrackInterfaces)

find_package(ROOT COMPONENTS MLP Graf Hist Matrix TreePlayer Gpad Graf3d TMVA)

gaudi_install_headers(FlavourTagging)


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DBOOST_LOG_DYN_LINK")

find_package(Boost COMPONENTS system filesystem unit_test_framework regex log
                              log_setup)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

file(GLOB_RECURSE FT_CPP_FILES "src/*.cpp")

gaudi_add_module(FlavourTagging
                 ${FT_CPP_FILES}
                 INCLUDE_DIRS Tr/TrackInterfaces
                 LINK_LIBRARIES PhysEvent GaudiPythonLib
                                DaVinciKernelLib LoKiPhysLib LoKiArrayFunctorsLib
                                ROOT Boost)

# Disable all but leak sanitizers for all factory compilations that include TMVA 
# auto-generated code, as these tend to explode compilation times..
if ( SANITIZER_ENABLED AND NOT SANITIZER_ENABLED STREQUAL "liblsan.so" )
  message(STATUS "Disabling ${SANITIZER_ENABLED} sanitizer for TMVA generated code.")
  file(GLOB_RECURSE FT_TMVA_FILES "src/Taggers/*.cpp")
  foreach(TMVA_FILE ${FT_TMVA_FILES})
    # This disables all sanitizers for all files under src/Taggers
    # Probably more than really required, but easiest for now.
    set_property(SOURCE ${TMVA_FILE} APPEND_STRING PROPERTY COMPILE_FLAGS " -fno-sanitize=all " )
  endforeach()
endif()

gaudi_add_executable(summaryof
                     summary/summaryof.cpp
                     INCLUDE_DIRS Tr/TrackInterfaces
                     LINK_LIBRARIES PhysEvent GaudiPythonLib
                                    DaVinciKernelLib LoKiPhysLib ROOT)

gaudi_install_python_modules()

gaudi_env(SET FLAVOURTAGGINGOPTS \${FLAVOURTAGGINGROOT}/options)

