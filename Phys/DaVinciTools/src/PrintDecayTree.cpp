/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/DaVinciAlgorithm.h"
#include "Kernel/IPrintDecay.h"
// ============================================================================
/** @class PrintDecayTree
 *  The simplified version of the algorithm PrintTree,
 *  which deals only with the reconstructed particles
 *  @see PrintTree
 *  @see IPRintDecay
 *  @author Vanya BELYAEV Ivan.Belayev@nikhef.nl
 *  @date 2008-03-30
 */
class PrintDecayTree : public DaVinciAlgorithm {
public:
  // ==========================================================================
  /// the standard execution of the algorithm
  StatusCode execute() override {
    // get the tool
    if ( !m_printDecay ) { m_printDecay = tool<IPrintDecay>( m_printDecayName, this ); }
    // get the particles
    const LHCb::Particle::ConstVector& parts = this->i_particles();
    //
    m_printDecay->printTree( parts.begin(), parts.end(), m_maxDepth );
    //
    setFilterPassed( m_forceFilterPassedTrue || !parts.empty() );
    //
    return StatusCode::SUCCESS;
  }
  // ==========================================================================
  /** the standard constructor
   *  @param name algorithm instance name
   *  @param pSvc service locator
   */
  PrintDecayTree( const std::string& name, ISvcLocator* pSvc )
      : DaVinciAlgorithm( name, pSvc )
      , m_printDecayName( "PrintDecayTreeTool/PrintDecay" )
      , m_printDecay( NULL )
      , m_maxDepth( 6 ) {
    declareProperty( "PrintDecayTool", m_printDecayName, "The type/name of the IPrintDecay tool" );
    declareProperty( "MaxDepth", m_maxDepth, "The maximal depth (number of levels)" );
    declareProperty( "ForceFilterPassed", m_forceFilterPassedTrue = false,
                     "Flag to turn on the forcing of filter passed to true always" );
  }

private:
  // ==========================================================================
  /// the type/name of the IPrintDecay tool
  std::string m_printDecayName; // the type/name of the IPrintDecay tool
  /// the IPrintDecay tool itself
  IPrintDecay* m_printDecay; // the IPrintDecay tool itself
  /// the maximal printout depth
  int m_maxDepth; // the maximal printout depth
  /// Force filter passed to true ?
  bool m_forceFilterPassedTrue;
  // ==========================================================================
};
// ============================================================================
/// declare the factory (needed for instantiation)
DECLARE_COMPONENT( PrintDecayTree )
// ============================================================================
// The END
// ============================================================================
