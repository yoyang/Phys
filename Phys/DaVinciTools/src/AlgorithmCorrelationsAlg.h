/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SELRESULTCOMBINATORICS_H
#define SELRESULTCOMBINATORICS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

struct IAlgorithmCorrelations;
struct ICheckSelResults;

/** @class AlgorithmCorrelationsAlg AlgorithmCorrelationsAlg.h
 *
 *  Produces a correlations between algorithms. Uses both
 *  filterPassed() and SelResult (which might be in DST)
 *
 *  Usage :
 *
 *  @code
 *  ApplicationMgr().TopAlg += [ AlgorithmCorrelationsAlg() ]
 *  AlgorithmCorrelationsAlg().Algorithms = [ "Hlt2Decision",
 *                                            "DiLeptonForPreselBu2LLK",
 *                                            "PreselBu2LLK" ]
 *  @endcode
 *
 *  @author Patrick KOPPENBURG
 *  @date   2004-09-01
 */
class AlgorithmCorrelationsAlg : public GaudiAlgorithm {
public:
  /// Standard constructor
  AlgorithmCorrelationsAlg( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~AlgorithmCorrelationsAlg(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
private:
  std::vector<std::string>        m_algorithmsRow;    ///< Algorithms to check
  std::vector<std::string>        m_algorithmsColumn; ///< Algorithms to against (= Row by default)
  std::vector<std::string>        m_algorithms;       ///< All Algorithms
  mutable IAlgorithmCorrelations* m_algoCorr;         ///< Correlation tool
  mutable ICheckSelResults*       m_selTool;          ///< Selection results tool

  bool m_printTable; ///< print output in Table format
  bool m_printList;  ///< print output in List format

  mutable Gaudi::Accumulators::Counter<unsigned int> m_eventsCounter{this, "Events"};
};

#endif // SELRESULTCOMBINATORICS_H
