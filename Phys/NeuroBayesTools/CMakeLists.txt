###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: NeuroBayesTools
################################################################################
gaudi_subdir(NeuroBayesTools)

gaudi_depends_on_subdirs(GaudiAlg
                         GaudiKernel
                         Phys/DaVinciKernel
                         Phys/LoKi
                         Phys/LoKiPhys)

find_package(NeuroBayesExpert)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

if(NeuroBayesExpert_FOUND)
  include_directories(${NEUROBAYESEXPERT_INCLUDE_DIRS})
  add_definitions(-DHAVE_NEUROBAYES)
endif()

gaudi_add_module(NeuroBayesTool
                 src/*.cpp
                 INCLUDE_DIRS AIDA Phys/LoKi
                 LINK_LIBRARIES GaudiAlgLib GaudiKernel DaVinciKernelLib LoKiPhysLib)

if(NeuroBayesExpert_FOUND)
  target_link_libraries(NeuroBayesTool ${NEUROBAYESEXPERT_LIBRARIES})
endif()

gaudi_install_python_modules()
