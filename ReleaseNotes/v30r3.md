2019-02-19 Phys v30r3
=====================

Development release prepared on the master branch.
It is based on Gaudi v31r0, LHCb v50r3, Lbcom v30r3 and Rec v30r3, and uses LCG_95 with ROOT 6.16.00.

- Fix for LHCBPS-1822
  - See merge request lhcb/Phys!495
- Make compatible with gaudi/Gaudi!834
  - See merge request lhcb/Phys!492
- Addition of new names for RelatedInfo enums
  - See merge request lhcb/Phys!490
- Addition of StdDiElectronLoose container with UU, LU and LL combinations for Bu2LLK stripping lines
  - See merge request lhcb/Phys!489
- Add new RelInfo keys for RelInfoTrackIsolationBDT2 tool
  - See merge request lhcb/Phys!486
- Directly request chi2/dof
  - See merge request lhcb/Phys!469
- Remove unused data member
  - See merge request lhcb/Phys!468
- Adapted to new Gaudi::Algorithm
  - See merge request lhcb/Phys!453
