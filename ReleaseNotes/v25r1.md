2018-04-11 Phys v25r1
=====================

Development release for 2018 data taking, prepared on the 2018-patches branch.
It is based on Gaudi v29r3, LHCb v44r1, Lbcom v22r0p1 and Rec v23r1 and uses LCG_93 with ROOT 6.12.06.

- Fix for bug found in AALLSAMEBPV functor
 - See merge request lhcb/Phys!303
- Fix HEPDrone when running from TCK
  - See merge request lhcb/Phys!313
- Add Particle to RelatedInfo cloner needed for Turbo processing
  - See merge request lhcb/Phys!302
- RestoreCaloRecoChain.cpp : 'refitDecayTree' option relies on default DVA particleCombiner
  instead of DecayTreeRefit to avoid issues when V0s are involved
  - See merge request lhcb/Phys!300
- Fix double free of inherited members (HEPDrone)
  - See merge request lhcb/Phys!298
- Move new PF related info tools at the end of RelatedInfoNamed.h list
  - See merge request lhcb/Phys!294
- Add distance-to-beamline functor (commit problems fix'd)
  - See merge request lhcb/Phys!281
- Bugfix and Document HEPDrone LHCBPS-1769
  - See merge request lhcb/Phys!290
- MomentumScaling : fix the error message, issued for correct 2017-turbo configuration
  - See merge request lhcb/Phys!289
- Add X,Y components for each variable in RelInfoPFVariables
  - See merge request lhcb/Phys!286
- PhysSelWrappers: allow extra keyword   arguments for AutomaticData (easy to debug...)
  - See merge request lhcb/Phys!283
