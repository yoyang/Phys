#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# test Selection-like classes
#
from PhysSelPython.Wrappers import (Selection, AutomaticData, MergedSelection,
                                    EventSelection, PassThroughSelection,
                                    NameError, NonEmptyInputLocations,
                                    IncompatibleInputLocations)
from PhysSelPython.MockStrippingLine import MockStrippingLine as dummy

from SelPy.configurabloids import MockConfGenerator, DummySequencer


def test_dummy_automaticdata():
    data = AutomaticData(Location='Phys/Data0')
    newData = dummy('NewData0', data)
    assert newData.selection().name() == 'NewData0'
    assert newData.outputLocation() == newData.selection().outputLocation(
    ) == 'Phys/NewData0/Particles'
    assert len(newData.members()) == 1
    print newData.members()[0].Members
    assert len(newData.members()[0].Members) == 2


def test_dummy_mergedselection():
    data0 = AutomaticData(Location='Phys/Data0')
    data1 = AutomaticData(Location='Phys/Data1')
    mergedData = MergedSelection(
        'MergedData01', RequiredSelections=[data0, data1])
    newData = dummy('NewMergedData', mergedData)
    assert newData.selection().name() == 'NewMergedData'
    assert newData.outputLocation() == newData.selection().outputLocation(
    ) == 'Phys/NewMergedData/Particles'
    assert len(newData.members()) == 1
    assert len(newData.members()[0].Members) == 3


def test_dummy_selection():
    data0 = AutomaticData(Location='Phys/Data0')
    data1 = AutomaticData(Location='Phys/Data1')
    algo = MockConfGenerator()
    sel = Selection('Sel01', Algorithm=algo, RequiredSelections=[data0, data1])
    newData = dummy('NewSel01', sel)
    assert newData.selection().name() == 'NewSel01'
    assert newData.outputLocation() == newData.selection().outputLocation(
    ) == 'Phys/NewSel01/Particles'
    assert len(newData.members()) == 3


def test_EventSelection():
    alg = MockConfGenerator()
    evtSel = EventSelection('DummyEvtSel', Algorithm=alg)
    newSel = dummy('NewEvtSel', evtSel)
    assert newSel.selection().name() == 'DummyEvtSel'
    assert newSel.outputLocation() == ''
    assert len(newSel.members()) == 1


def test_PassThroughSelection():
    alg = MockConfGenerator()
    data0 = AutomaticData(Location='Phys/Data0')
    evtSel = PassThroughSelection(
        'TestPassThrough', Algorithm=alg, RequiredSelection=data0)
    newSel = dummy('NewEvtSel', evtSel)
    assert newSel.selection().name() == 'NewEvtSel'
    assert newSel.outputLocation() == 'Phys/NewEvtSel/Particles'
    assert len(newSel.members()) == 1


if __name__ == '__main__':
    import nose
    from os.path import basename, splitext
    nose.run(defaultTest=splitext(basename(__file__))[0])
