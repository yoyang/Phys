###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from nose.tools import raises

from PhysSelPython import __sanity__

from Configurables import FilterDesktop


def test_filterDesktop_properties():
    fd0 = FilterDesktop(
        'fd0', Code='TestCode0', InputLocations=['a', 'b', 'c'], OutputLevel=2)

    assert (fd0.Code == 'TestCode0')
    assert (fd0.InputLocations == ['a', 'b', 'c'])
    assert (fd0.OutputLevel == 2)


@raises(ValueError)
def test_duplicate_name_raises():
    fd1 = FilterDesktop(
        'fd1', Code='TestCode1', InputLocations=['d', 'e', 'f'], OutputLevel=2)

    FilterDesktop(
        'fd1', Code='TestCode1', InputLocations=['d', 'e', 'f'], OutputLevel=2)
