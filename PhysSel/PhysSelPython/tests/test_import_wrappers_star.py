#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Test loading of module * at top level.
'''


def test_import_physselpython_wrappers_star():
    from PhysSelPython.Wrappers import *


if __name__ == '__main__':
    import nose
    from os.path import basename, splitext
    nose.run(defaultTest=splitext(basename(__file__))[0])
