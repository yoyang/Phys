###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Module containing fixed versions of insane configurables.
"""

__author__ = 'Juan Palacios palacios@physik.uzh.ch'

from ConfigurableHospital import memoized_sanitise
from CrazyConfigurables import configurables


def addConfigurables(configurables):
    sanitise(configurables)


from sys import modules
this = modules[__name__]
import Configurables as _crazyConfigurables
sanitise = memoized_sanitise(_crazyConfigurables, this)
sanitise(configurables())


def cureConfigurables(configurables):
    """
    Take configurables, cure them, and add them to this module.

    """
    sanitise(configurables)
